Introduction
============

Install
-------

Import this library in the user script of your *Bliss* session

.. code-block:: python

    from id21blissutils import *
    id21blissutils_setup()

Get help
--------

Get help on the API available in a *Bliss* session:

.. code-block:: python

    SESSION_NAME [1]: helpall()
    SESSION_NAME [2]: helpid21()

External HDF5 writer
--------------------

This writer will save scans published by *Bliss* in HDF5 files under `Nexus conventions
<https://www.nexusformat.org/>`_ (see also `ESRF Nexus conventions
<https://gitlab.esrf.fr/sole/data_policy/blob/master/ESRF_NeXusImplementation.rst>`_). To attach the writer to a *Bliss* session with name *session_name*:

.. code-block:: bash

    python -m id21blissutils.server.nxscanwriter session_name --log=info

The python process has one main async thread which listens to the session events in Redis. For each scan, a separate async thread is launched to save the associated data as an NXentry in the current dataset's HDF5 file.

Dataset management
------------------

The ESRF data policy requires data to be grouped in *datasets*. Locally these datasets have the following path template

.. code-block:: bash

    <root>/<experiment>/<beamline>/<sample>/<dataset>

Data from scans are saved under this directory.

Change experiment
+++++++++++++++++

The *root*, *experiment* and *beamline* can be changed in the *Bliss* session with the commands *newinhouse*, *newvisitor* or *newexperiment*. For example

.. code-block:: python

    SESSION_NAME [3]: newvisitor('hg94')

Additional information can be specified:

.. code-block:: python

    SESSION_NAME [4]: newvisitor('hg94', bl='id21', sample='sample1', dataset='area1', technique='xrf')

When the *bl* argument is missing, the name of the beamline is retrieved in the following order:

 * *bl* argument from last call to *newinhouse*, *newvisitor* or *newexperiment*
 * environment variable *BEAMLINE*
 * environment variable *BEAMLINENAME*
 * static Bliss session configuration (yaml) variable *beamline*
 * id00

The *root* depends on which command is used to switch experiments:

 * *newvisitor*: :code:`root = /data/visitor`
 * *newinhouse*: :code:`root = /data/id21/inhouse/19aug` (depending on the year and month)
 * *newexperiment*: :code:`root = /data/id21/tmp` or specified with argument *root*

The command *closeexperiment* changes to the beamline's monthly default experiment with :code:`<root>/<experiment> = /data/id21/inhouse/default/id211908` (depending on the year and month). This is equivalent to calling :code:`newinhouse(None, ...)`.


Change sample
+++++++++++++

The sample name can be changed by

.. code-block:: python

    SESSION_NAME [5]: newsample('gypsum')

Additional information can be specified:

.. code-block:: python

    SESSION_NAME [6]: newsample('gypsum', dataset='area1', technique='xrf')

Samples have metadata associated to them, which can be defined as follows:

.. code-block:: python

    TEST_SESSION_1 [7]: definesample('gypsum', description='This is a Gypsum standard', formula='CaSO4(H20)2')
    TEST_SESSION_1 [8]: definesample('calcite', description='This is a Calcite standard', formula='CaCO3')
    TEST_SESSION_1 [9]: samples()
    
     Name       : gypsum
     Description: This is a Gypsum standard
     Formula    : CaSO4(H20)2
     Datasets   : []

     Name       : calcite
     Description: This is a Calcite standard
     Formula    : CaCO3
     Datasets   : []

Datasets that have been recorded for each sample are listed.

Change dataset
++++++++++++++

The active dataset can be change by

.. code-block:: python

    SESSION_NAME [10]: newdataset()

Both the internal and external Bliss writer will save their data under the last specified dataset. Additional information can be specified:

.. code-block:: python

    SESSION_NAME [11]: newdataset('area1', technique='xrf')

When no dataset name is specified, a number will be used ('0001', '0002', etc.). Due to the data policy, the sample name is automatically added as a prefix to the dataset name. So the dataset folder name will be 'sample1_0001', 'sample1_area1', etc. The available techniques are specified in the static Bliss session configuration (yaml):

.. code-block:: python

    SESSION_NAME [12]: techniques()
             Out [12]: ['xrf', 'xas', 'xrfxrd', 'fullfield']

ICAT
++++

To register your datasets so they are available in https://data.esrf.fr/, the Bliss session needs to be attached to a `metadataManager tango server <https://gitlab.esrf.fr/icat/tango-metadata>`_

.. code-block:: python

    SESSION_NAME [13]: mdataon()

Whenever you call dataset management functions like *newvisitor* and *newsample*, information will be synchronized with the *metadataManager* and messages will be send to the electronic logbook. The static Bliss session configuration allows the selection of one `metadataManager` per technique

.. code-block:: yaml

    name: scan_info
    mdata:
      default: tango://lid21sxm:20000/blisstest/metadata/ing-blisstest
      xrf: tango://lid21sxm:20000/blisstest/metadata/ing-blisstest

Example
+++++++
Start a new inhouse experiment with *sample1* and dataset *area1*:

.. code-block:: python

    SESSION_NAME [14]: newinhouse('blc1234')
    SESSION_NAME [15]: newsample('sample1')
    SESSION_NAME [16]: newdataset('area1')
    SESSION_NAME [17]: saveinfo()
    Bliss saving info : 
    -------------------
    Status         : COMPLETE
    Path           : /data/id21/inhouse/19aug/blc1234/id21/sample1/sample1_area1
    Template       : {experiment}/{beamline}/{sample}/{dataset}
    Base_path      : /data/id21/inhouse/19aug
    Experiment     : id211908
    Beamline       : id21
    Technique      : xrf
    Sample         : sample1
    Dataset        : sample1_area1
    Metadatamanager: ENABLED (SYNCHRONIZED)
    -------------------

Start a new test experiment, not connected to ICAT (default sample and dataset):

.. code-block:: python

    SESSION_NAME [18]: mdataoff()
    SESSION_NAME [19]: newexperiment()
    SESSION_NAME [20]: saveinfo()
    Bliss saving info : 
    -------------------
     Status         : COMPLETE
     Path           : /data/id21/tmp/proposalvFUZu4/id21/default/0001
     Template       : {experiment}/{beamline}/{sample}/{dataset}
     Proposal       : proposalvFUZu4
     Root           : /data/id21/tmp
     Beamline       : id21
     Sample         : default
     Dataset        : 0001
     Technique      : xrf
     Metadatamanager: DISABLED (NOT SYNCHRONIZED)
    -------------------

Nexus configuration
+++++++++++++++++++

Nexus plots and application definitions can be specified for each technique in the static Bliss session configuration:

.. code-block:: yaml

    name: scan_info
    technique:
      default: xrf
      techniques:
        xrf:
          applications:
          - xrf
          plots:
          - xrf_counters
          - xrf_spectra
        xas:
          applications:
          - xrf
          plots:
          - xas_counters
          - xrf_spectra
        xrfxrd:
          applications:
          - xrf
          plots:
          - xrf_counters
          - xrf_spectra
          - xrd
        fullfield:
          applications: []
          plots:
          - fullfield

When a technique does not have any plots associated to it, all data will be selected for plotting, grouped by dimensionality

 * 0D: counters (e.g. diode, sensor, ...)
 * 1D: spectra (e.g. mca)
 * 2D: images (e.g. lima)

`Plots <http://download.nexusformat.org/doc/html/classes/base_classes/NXdata.html#nxdata>`_ are defined as

.. code-block:: yaml

    name: scan_info
    technique:
      plots:
        xas_counters:
          personal_name: counters
          items:
          - simabsorbance
          - simfdetnorm
          - simxmap1:deadtime_det0
          - simxmap2:deadtime_det1
        xrf_counters:
          personal_name: counters
          items:
          - simidetnorm
          - simfdetnorm
          - simxmap1:deadtime_det0
          - simxmap2:deadtime_det1
        xrf_spectra:
          personal_name: spectra
          items:
          - simxmap1:spectrum_det0
          - simxmap2:spectrum_det1
        xrd:
          personal_name: patterns
          items:
          - simfrelon:image
        fullfield:
          personal_name: images
          items:
          - simpco:image

`Application definitions <http://download.nexusformat.org/doc/html/classes/applications/index.html>`_ are defined as

.. code-block:: yaml

    name: scan_info
    technique:
      applications:
        xrf:
          personal_name: xrf
          class: APPxrf
          I0: simiodet
          It: simidet
          mca:
          - simxmap1:det0
          - simxmap2:det1

Currently no definition language exists so the writer needs to know the application definitions a-priori.