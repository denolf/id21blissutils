from setuptools import setup, find_packages
import os

about = {}
root = os.path.dirname(os.path.realpath(__file__))
with open(os.path.join(root, 'id21blissutils', '__about__.py')) as fp:
    tmp = {}
    exec(fp.read(), tmp, about)

install_requires = [
    'numpy',
    'h5py',
    'silx',
    'bliss',
    'pyparsing'
]

setup(
    name=about['__project__'],
    version=about['__version__'],
    description=about['__description__'],
    url=about['__url__'],
    packages=find_packages(),
    install_requires=install_requires,
    test_suite='id21blissutils.tests.test_all.test_suite'
)
