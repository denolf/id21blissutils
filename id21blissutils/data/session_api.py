"""
Data API exposed to the Bliss session
"""

from contextlib import contextmanager
from nexus_writer_service.metadata import register_all_metadata_generators
from nexus_writer_service.utils import data_policy
from . import blissdata
from ..utils import config
from . import metadata
from . import metasync
from . import sample_catalogue


__all__ = ['newvisitor', 'newinhouse', 'newlocalexperiment',
           'newdefaultexperiment', 'newtmpexperiment',
           'closeexperiment', 'newsample', 'newdataset',
           'switchtechnique', 'saveinfo', 'msaveinfo',
           'mdataon', 'mdataoff', 'mdatapull', 'mdatapush',
           'mdatacompare', 'elog', 'definesample', 'sample',
           'samples']


def _before_new_experiment():
    """
    Cleanup previous experiment
    """
    # Stop the previous dataset (if running)
    metadata.enddataset()
    # In the MetadataManager is enabled, check whether the
    # path template is the same in Bliss and MetadataManager
    metasync.assertSynchronized('template')
    # Remove the previous sample catalogue
    sample_catalogue.remove_catalogue()


def _after_new_experiment(proposal_type='', **kwargs):
    """
    Notify MetadataManager and e-logbook

    :param str proposal_type: 'visitor' or 'inhouse'
    :param **kwargs: arguments for `newsample`
    """
    # Synchronize with MetadataManager (if enabled)
    metasync.pushpull('experiment', 'base_path')
    # Send message to e-logbook (if enabled)
    if proposal_type:
        proposal_type = proposal_type + ' experiment'
    proposal = config.scan_saving_get('experiment')
    metadata.elogmessage("New {} '{}'"
                         .format(proposal_type, proposal),
                         type='info')
    newsample(**kwargs)


@contextmanager
def _new_experiment(proposal_type=None, sample=None, dataset=None,
                       technique=None):
    """
    Switch to user experiment

    :param str proposal_type: for logbook
    :param str sample: keep current by default ('default' when missing)
    :param str dataset: by default a number starting from 1
    :param str technique: keep current by default
    """
    _before_new_experiment()
    yield
    _after_new_experiment(proposal_type='visitor', name=sample,
                          dataset=dataset, technique=technique)


def newvisitor(proposal, sample=None, dataset=None, technique=None):
    """
    Switch to user experiment

    :param str proposal: e.g. 'hg94'
    :param str sample: keep current by default ('default' when missing)
    :param str dataset: by default a number starting from 1
    :param str technique: keep current by default
    """
    with _new_experiment(proposal_type='visitor', sample=sample,
                         dataset=dataset, technique=technique):
        data_policy.newvisitor(proposal)


def newinhouse(proposal, sample=None, dataset=None, technique=None):
    """
    Switch to inhouse experiment

    :param str proposal: e.g. 'blc10696' (default beamline session of
                                          the month when `None`)
    :param str sample: keep current by default ('default' when missing)
    :param str dataset: by default a number starting from 1
    :param str technique: keep current by default
    """
    with _new_experiment(proposal_type='inhouse', sample=sample,
                         dataset=dataset, technique=technique):
        data_policy.newinhouse(proposal)


def newdefaultexperiment(sample=None, dataset=None, technique=None):
    """
    Switch to inhouse experiment

    :param str sample: keep current by default ('default' when missing)
    :param str dataset: by default a number starting from 1
    :param str technique: keep current by default
    """
    with _new_experiment(proposal_type='inhouse', sample=sample,
                         dataset=dataset, technique=technique):
        data_policy.newdefaultexperiment()


def newtmpexperiment(proposal=None, root=None, sample=None,
                     dataset=None, technique=None):
    """
    Switch to experiment

    :param str proposal: random name when missing
    :param str sample: keep current by default ('default' when missing)
    :param str dataset: by default a number starting from 1
    :param str technique: keep current by default
    """
    with _new_experiment(proposal_type='tmp', sample=sample,
                         dataset=dataset, technique=technique):
        data_policy.newtmpexperiment(proposal)


def newlocalexperiment(proposal=None, root=None, sample=None,
                       dataset=None, technique=None):
    """
    Switch to experiment

    :param str proposal: random name when missing
    :param str root: proposal root directory (e.g. '/data/id00/tmp')
    :param str sample: keep current by default ('default' when missing)
    :param str dataset: by default a number starting from 1
    :param str technique: keep current by default
    """
    with _new_experiment(proposal_type='local', sample=sample,
                         dataset=dataset, technique=technique):
        data_policy.newlocalexperiment(proposal, root=root)


def closeexperiment(**kwargs):
    """
    This is the same as calling `newdefaultexperiment`.

    :param str **kwargs: see `newdefaultexperiment`
    """
    newdefaultexperiment(**kwargs)


def newsample(name=None, dataset=None, technique=None):
    """
    Change sample

    :param str name: keep current by default ('default' when missing)
    :param str dataset: by default a number starting from 1
    :param str technique: keep current by default
    """
    metadata.enddataset()
    metasync.assertSynchronized('template', 'experiment', 'base_path',
                                'beamline')
    blissdata.newsample(name=name)
    metasync.pushpull('sample')
    description = sample_catalogue.definition().description
    if not description:
        description = 'not in catalogue'
    metadata.elogmessage("New sample '{}' ({})"
                         .format(config.scan_saving_get('sample'),
                                 description),
                         type='info')
    newdataset(name=dataset, technique=technique)


def definesample(name=None, **kwargs):
    """
    Define a sample

    :param str name: sample name
    :param **kwargs: sample attributes
    """
    sample_catalogue.define(name=name, **kwargs)
    metasync.pushmeta()


def newdataset(name=None, technique=None):
    """
    Change dataset

    :param str or num name:
    :param str technique: keep current by default
    """
    metadata.enddataset()
    metasync.assertSynchronized('template', 'experiment', 'base_path',
                                'beamline', 'sample')
    blissdata.newdataset(name=name, nonexisting=True)
    metasync.pushpull('dataset')
    metadata.startdataset()
    switchtechnique(name=technique)
    metadata.elogmessage("New dataset '{}' ({})"
                         .format(config.scan_saving_get('dataset'),
                                 config.scan_saving_get('technique')),
                         type='info')


def switchtechnique(name=None):
    """
    Change technique

    :param str name: keep current by default ('unknown' when missing)
    """
    metasync.assertSynchronized('template', 'experiment', 'base_path', 'beamline',
                                'sample', 'dataset')
    blissdata.switchtechnique(name=name)
    metasync.pushpull('technique')
    # Initialize Redis metadata generators
    # (if not already done by the setup script)
    register_all_metadata_generators()


def _formatdict(info, title):
    """
    :param dict info:
    :returns list(str):
    """
    lines = []
    n = max(len(k) for k in info)
    if title:
        n = max(n, len(title))
        line = '-' * (n + 2)
    fmt = '{{:{}}} : {{}}'.format(n)
    if title:
        lines.append(fmt.format(title, ''))
    n = max(map(len, info.keys()))
    fmt = ' {{:{}}}: {{}}'.format(n)
    if title:
        lines.append(line)
    for k, v in info.items():
        lines.append(fmt.format(k.capitalize(), v))
    if title:
        lines.append(line)
    return lines


def _sync_info(info):
    if metasync.synchronized(allow_disabled=True):
        equal = 'SYNCHRONIZED'
    else:
        equal = 'NOT SYNCHRONIZED'
    if metasync.enabled():
        state = 'ENABLED'
    else:
        state = 'DISABLED'
    info['MetadataManager'] = '{} ({})'.format(state, equal)


def saveinfo(show=True):
    """
    Print Bliss saving information

    :param bool show: print
    :returns list(str) or None:
    """
    lines = ['\n']
    info = blissdata.saveinfo()
    _sync_info(info)
    if info:
        lines += _formatdict(info, 'Bliss saving info')
    if show:
        for line in lines:
            print(line)
    else:
        return lines


def msaveinfo(show=True):
    """
    Print MetadataManager saving information

    :param bool show: print
    :returns list(str) or None:
    """
    lines = ['\n']
    info = metadata.saveinfo()
    _sync_info(info)
    if info:
        lines += _formatdict(info, 'MetadataManager saving info')
    if show:
        for line in lines:
            print(line)
    else:
        return lines


def mdataon(push=True, reset=None, validate=False):
    """
    Enable metadata and synchronize (push or pull)

    :param bool push:
    :param str reset: Reset the MetadataManager first ('soft', 'hard')
    :param bool validate: validate by MetadataClient
    """
    metadata.enable(push=push, reset=reset, validate=validate)


def mdataoff(close=True):
    """
    Disable metadata

    :param bool close: End the running dataset first
    """
    metadata.disable(close=close)


def mdatapull(allow_disabled=True, validate=False):
    """
    From Metadata servers to SCAN_SAVING

    :param bool allow_disabled:
    :param bool validate: validate by MetadataClient
    """
    metasync.pull(allow_disabled=allow_disabled, validate=validate)


def mdatapush(allow_disabled=False, reset=None, validate=False):
    """
    From SCAN_SAVING to Metadata servers

    :param bool allow_disabled:
    :param str reset: Reset the MetadataManager first ('soft', 'hard')
    :param bool validate: validate by MetadataClient
    """
    if reset == 'soft' or reset == 'hard':
        metasync.reset(force=reset == 'hard')
    metasync.push(allow_disabled=allow_disabled, validate=validate)


def mdatareset(allow_disabled=False):
    """
    Reset experiment in MetadataManager (proposal, sample, dataset)
    """
    metadata.resetexperiment(allow_disabled=allow_disabled)


def mdatacompare(show=True):
    """
    Compare SCAN_SAVING and Metadata servers
    """
    kwargs = {'allow_disabled': True, 'raise_on_error': False}
    blissdict = metasync.attribute_dict(**kwargs)
    metadict = metasync.attribute_dict(other=True, **kwargs)
    issynchronized = True
    lines = []
    for (blissattr, blissvalue), (metaattr, metavalue) in\
        zip(blissdict.items(), metadict.items()):
        if blissvalue == metavalue:
            lines.append('{} = {}'.format(blissattr, blissvalue))
        else:
            issynchronized = False
            lines.append('{}: NOT EQUAL\n Bliss = {}\n MetadataManagers = {}'
                         .format(blissattr, blissvalue, metavalue))
    if issynchronized:
        lines.append('SYNCHRONIZED')
    else:
        lines.append('NOT SYNCHRONIZED')
    if show:
        for line in lines:
            print(line)
    else:
        return lines


def elog(*msg):
    """
    Write in the electronic logbook
    """
    metadata.elogmessage(*msg, type='user')


def sample(name=None, show=True):
    """
    Print sample definition

    :param bool show: print
    :returns list(str) or None:
    """
    lines = ['\n']
    info = sample_catalogue.definition(name=name)
    if info:
        info = info.to_dict()
        info = {k: v for k, v in info.items() if not k.startswith('_')}
        name = config.scan_saving_get('sample', '')
        lines += _formatdict(info, 'Sample ' + repr(name))
    if show:
        for line in lines:
            print(line)
    else:
        return lines


def samples(show=True):
    """
    All saved and defined samples
    """
    savednames = set(config.samples())
    catnames = set(sample_catalogue.catalogue().instances)
    allnames = savednames | catnames
    lines = []
    for name in sorted(allnames):
        definition = sample_catalogue.definition(name=name).to_dict()
        info = {'name': name}
        for k, v in definition.items():
            if not k.startswith('_'):
                info[k] = v
        info['datasets'] = str(config.datasets(name))
        lines.append('')
        lines += _formatdict(info, None)
    if show:
        for line in lines:
            print(line)
    else:
        return lines
