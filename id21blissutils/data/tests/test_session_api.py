import unittest
import os
import re
from ...testing.unittest import BlissTestCase
from ...io.utils import mkdir, tempname
from .. import metasync
from .. import sample_catalogue


anydir = '[^{}]+'.format(os.sep)


class test_session_api(BlissTestCase):

    def setUp(self):
        super(test_session_api, self).setUp(xrd=False,
                                            fullfield=False,
                                            managed=True)

    @property
    def save_info(self):
        return self.session_env.SCAN_SAVING

    def _init_experiment(self, proposal=None):
        self.save_info.beamline = 'id21'
        self.session_env.newlocalexperiment(technique='xrf',
                                            proposal=proposal,
                                            root=self.dir.path)

    def assertPath(self, pattern):
        path = self.save_info.get_path()
        self.assertTrue(re.match(pattern, path), (path, pattern))

    def test_experiment(self):
        self._init_experiment()
        pattern = os.path.join(self.dir.path, anydir, 'id21',
                               'default', 'default_0001')
        self.assertPath(pattern)
        self._init_experiment(proposal='id210000')
        pattern = os.path.join(self.dir.path, 'id210000', 'id21',
                               'default', 'default_0001')
        self.assertPath(pattern)

    def test_sample(self):
        env = self.session_env
        self._init_experiment()
        self.assertEqual(self.save_info.sample, 'default')
        env.newsample('abc', 'description')
        self.assertEqual(self.save_info.sample, 'abc')

    def test_dataset(self):
        env = self.session_env
        self._init_experiment()
        self.assertEqual(self.save_info.dataset, '0001')
        env.newdataset('abc')
        self.assertEqual(self.save_info.dataset, 'abc')

    def test_saveinfo(self):
        env = self.session_env
        self._init_experiment()
        env.saveinfo(show=False)
        env.msaveinfo(show=False)

    def test_mdatasync(self):
        env = self.session_env
        self._init_experiment(proposal='id210000')

        env.mdataon(push=True, reset='hard')
        self.assertEqual(self.save_info.dataset, '0001')
        self.assertTrue(metasync.synchronized())
        env.newdataset('area1')
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, 'area1')
        self.assertTrue(metasync.synchronized())
        env.newsample('abc')
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.sample, 'abc')
        self.assertEqual(self.save_info.dataset, '0001')
        self.assertTrue(metasync.synchronized())

        env.mdataoff()
        env.newdataset('area2')
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, 'area2')
        self.assertFalse(metasync.synchronized(allow_disabled=True))

        env.mdataon(push=False)
        self.assertTrue(metasync.synchronized())
        self.assertEqual(metasync.metaproxy().dataset, '')

        env.mdataoff()
        env.newdataset('area3')
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, 'area3')
        self.assertFalse(metasync.synchronized(allow_disabled=True))

        env.mdataon(push=True)
        self.assertTrue(metasync.synchronized())
        self.assertEqual(metasync.metaproxy().dataset, 'area3')

    def test_auto_datasets(self):
        env = self.session_env
        self._init_experiment(proposal='id210000')
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, '0001')
        env.newdataset()
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, '0002')
        env.newdataset(10)
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, '0010')
        env.newdataset(5)
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, '0011')
        env.newdataset('area1')
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, 'area1')
        env.newdataset('area1')
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, 'area1_0001')

        env.mdataon(push=True, reset='hard')
        env.newsample('sample1')
        mkdir(self.save_info.get_path())
        self.assertTrue(metasync.synchronized())
        self.assertEqual(self.save_info.dataset, '0001')
        env.newdataset()
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, '0002')
        env.newdataset(10)
        mkdir(self.save_info.get_path())
        self.assertTrue(metasync.synchronized())
        self.assertEqual(self.save_info.dataset, '0010')
        env.newdataset(5)
        mkdir(self.save_info.get_path())
        self.assertTrue(metasync.synchronized())
        self.assertEqual(self.save_info.dataset, '0011')
        env.newdataset(11)
        mkdir(self.save_info.get_path())
        self.assertTrue(metasync.synchronized())
        self.assertEqual(self.save_info.dataset, '0012')
        env.newdataset('area1')
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, 'area1')
        env.newdataset('area1')
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, 'area1_0001')

        env.mdataoff()
        env.newdataset()
        mkdir(self.save_info.get_path())
        self.assertFalse(metasync.synchronized(allow_disabled=True))
        self.assertEqual(self.save_info.dataset, '0013')
        env.newdataset(11)
        mkdir(self.save_info.get_path())
        self.assertEqual(self.save_info.dataset, '0014')

    def test_sample_catalogue(self):
        env = self.session_env
        self._init_experiment()
        sample1 = {'description': 'ABC', 'formula': 'CaCO3'}
        sampleu = {'description': '', 'formula': ''}
        env.definesample('sample1', description='ABC', formula='CaCO3', edit=False)
        env.definesample('sample3', description='DEF', edit=False)
        env.newsample('sample1')
        self.assertEqual(sample_catalogue.definition().to_dict(), sample1)
        env.newsample('sample2')
        self.assertEqual(sample_catalogue.definition().to_dict(), sampleu)
        env.newsample('sample1')
        env.mdataon(push=True, reset='hard')
        env.newsample('sample3')
        self.assertTrue(metasync.synchronized())


def test_suite():
    """Test suite including all test suites"""
    testSuite = unittest.TestSuite()
    testSuite.addTest(test_session_api("test_experiment"))
    testSuite.addTest(test_session_api("test_sample"))
    testSuite.addTest(test_session_api("test_dataset"))
    testSuite.addTest(test_session_api("test_saveinfo"))
    testSuite.addTest(test_session_api("test_mdatasync"))
    testSuite.addTest(test_session_api("test_auto_datasets"))
    testSuite.addTest(test_session_api("test_sample_catalogue"))
    return testSuite


if __name__ == '__main__':
    import sys

    mysuite = test_suite()
    runner = unittest.TextTestRunner()
    if not runner.run(mysuite).wasSuccessful():
        sys.exit(1)
