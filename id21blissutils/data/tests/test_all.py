import unittest
from . import test_naming
from . import test_compounds
from . import test_blissdata
from . import test_session_api


def test_suite():
    """Test suite including all test suites"""
    testSuite = unittest.TestSuite()
    testSuite.addTest(test_naming.test_suite())
    testSuite.addTest(test_compounds.test_suite())
    testSuite.addTest(test_blissdata.test_suite())
    testSuite.addTest(test_session_api.test_suite())
    return testSuite


if __name__ == '__main__':
    import sys

    mysuite = test_suite()
    runner = unittest.TextTestRunner()
    if not runner.run(mysuite).wasSuccessful():
        sys.exit(1)
