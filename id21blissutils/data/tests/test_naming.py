import unittest
from testfixtures import TempDirectory
import os
from .. import naming
from ...io.utils import mkdir


class test_naming(unittest.TestCase):

    def setUp(self):
        self.dir = TempDirectory()

    def tearDown(self):
        self.dir.cleanup()

    def test_nextpath(self):
        def pathgen(name):
            return os.path.join(self.dir.path,
                                'abc_{}_def'.format(name),
                                'sample_{}'.format(name))
        name, fmt = naming.nextpath(pathgen, 0)
        self.assertEqual(fmt.format(name), '0001')
        name, fmt = naming.nextpath(pathgen, 10)
        mkdir(pathgen(fmt.format(name)))
        name, fmt = naming.nextpath(pathgen, 1)
        self.assertEqual(fmt.format(name), '0011')
        mkdir(pathgen(fmt.format(name)))
        name, fmt = naming.nextpath(pathgen, '0005')
        self.assertEqual(fmt.format(name), '0012')
        mkdir(pathgen(fmt.format(name)))
        name, fmt = naming.nextpath(pathgen, '00005')
        self.assertEqual(fmt.format(name), '00013')
        mkdir(pathgen(fmt.format(name)))

        name, fmt = naming.nextpath(pathgen, 'name')
        self.assertEqual(fmt.format(name), 'name')
        mkdir(pathgen(fmt.format(name)))
        name, fmt = naming.nextpath(pathgen, 'name')
        self.assertEqual(fmt.format(name), 'name_0001')
        mkdir(pathgen(fmt.format(name)))
        name, fmt = naming.nextpath(pathgen, 'name')
        self.assertEqual(fmt.format(name), 'name_0002')
        mkdir(pathgen(fmt.format(name)))
        mkdir(pathgen('name_0010'))
        name, fmt = naming.nextpath(pathgen, 'name')
        self.assertEqual(fmt.format(name), 'name_0011')
        mkdir(pathgen(fmt.format(name)))

    def test_dataset_name(self):
        template = os.path.join(self.dir.path, '{experiment}',
                                '{sample}', '{dataset}', 'abc_{dataset}')
        variables = {'experiment': 'hg94',
                     'sample': 'sample1',
                     'dataset': 'area1'}

        variables['dataset'] = naming.dataset_name(None, template,
                                                  variables,
                                                  sampleprefix=False)
        path = template.format(**variables)
        epath = os.path.join(self.dir.path, 'hg94',
                             'sample1', '0001', 'abc_0001')
        self.assertEqual(path, epath)
        self.assertEqual(variables['dataset'], '0001')
        mkdir(path)

        variables['dataset'] = naming.dataset_name(None, template,
                                                  variables,
                                                  sampleprefix=False)
        path = template.format(**variables)
        epath = os.path.join(self.dir.path, 'hg94',
                             'sample1', '0002', 'abc_0002')
        self.assertEqual(path, epath)
        self.assertEqual(variables['dataset'], '0002')
        mkdir(path)

        variables['dataset'] = naming.dataset_name(None, template,
                                                  variables,
                                                  sampleprefix=True)
        path = template.format(**variables)
        epath = os.path.join(self.dir.path, 'hg94',
                             'sample1', 'sample1_0001', 'abc_sample1_0001')
        self.assertEqual(path, epath)
        self.assertEqual(variables['dataset'], 'sample1_0001')
        mkdir(path)

        variables['dataset'] = naming.dataset_name(None, template,
                                                  variables,
                                                  sampleprefix=True)
        path = template.format(**variables)
        epath = os.path.join(self.dir.path, 'hg94',
                             'sample1', 'sample1_0002', 'abc_sample1_0002')
        self.assertEqual(path, epath)
        self.assertEqual(variables['dataset'], 'sample1_0002')
        mkdir(path)

        variables['dataset'] = naming.dataset_name(10, template,
                                                  variables,
                                                  sampleprefix=True)
        path = template.format(**variables)
        epath = os.path.join(self.dir.path, 'hg94',
                             'sample1', 'sample1_0010', 'abc_sample1_0010')
        self.assertEqual(path, epath)
        self.assertEqual(variables['dataset'], 'sample1_0010')
        mkdir(path)

        variables['dataset'] = naming.dataset_name(5, template,
                                                  variables,
                                                  sampleprefix=True)
        path = template.format(**variables)
        epath = os.path.join(self.dir.path, 'hg94',
                             'sample1', 'sample1_0011', 'abc_sample1_0011')
        self.assertEqual(path, epath)
        self.assertEqual(variables['dataset'], 'sample1_0011')
        mkdir(path)

        variables['dataset'] = naming.dataset_name('area1', template,
                                                  variables,
                                                  sampleprefix=True)
        path = template.format(**variables)
        epath = os.path.join(self.dir.path, 'hg94',
                             'sample1', 'sample1_area1', 'abc_sample1_area1')
        self.assertEqual(path, epath)
        self.assertEqual(variables['dataset'], 'sample1_area1')
        mkdir(path)

        variables['dataset'] = naming.dataset_name('area1', template,
                                                  variables,
                                                  sampleprefix=True,
                                                  nonexisting=False)
        path = template.format(**variables)
        epath = os.path.join(self.dir.path, 'hg94',
                             'sample1', 'sample1_area1', 'abc_sample1_area1')
        self.assertEqual(path, epath)
        self.assertEqual(variables['dataset'], 'sample1_area1')
        mkdir(path)

        variables['dataset'] = naming.dataset_name('area1', template,
                                                  variables,
                                                  sampleprefix=True)
        path = template.format(**variables)
        epath = os.path.join(self.dir.path, 'hg94',
                             'sample1', 'sample1_area1_0001',
                             'abc_sample1_area1_0001')
        self.assertEqual(path, epath)
        self.assertEqual(variables['dataset'], 'sample1_area1_0001')
        mkdir(path)

        variables['dataset'] = naming.dataset_name('area1', template,
                                                  variables,
                                                  sampleprefix=True)
        path = template.format(**variables)
        epath = os.path.join(self.dir.path, 'hg94',
                             'sample1', 'sample1_area1_0002',
                             'abc_sample1_area1_0002')
        self.assertEqual(path, epath)
        self.assertEqual(variables['dataset'], 'sample1_area1_0002')
        mkdir(path)


def test_suite():
    """Test suite including all test suites"""
    testSuite = unittest.TestSuite()
    testSuite.addTest(test_naming('test_nextpath'))
    testSuite.addTest(test_naming('test_dataset_name'))
    return testSuite


if __name__ == '__main__':
    import sys

    mysuite = test_suite()
    runner = unittest.TextTestRunner()
    if not runner.run(mysuite).wasSuccessful():
        sys.exit(1)
