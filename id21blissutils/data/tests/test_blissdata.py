import unittest
import os
import re
from ...testing.unittest import BlissTestCase
from .. import blissdata
from ...io.utils import mkdir


anydir = '[^{}]+'.format(os.sep)


class test_blissdata(BlissTestCase):

    def setUp(self):
        super(test_blissdata, self).setUp(xrd=False,
                                          fullfield=False,
                                          managed=True)

    @property
    def save_info(self):
        return self.session_env.SCAN_SAVING

    def assertPath(self, pattern):
        path = self.save_info.get_path()
        self.assertTrue(re.match(pattern, path), (path, pattern))

    def test_experiment(self):
        pattern = os.path.join(self.dir.path, anydir, 'id00',
                               'default', 'default_0001')
        self.assertPath(pattern)

    def test_sample(self):
        save_info = self.save_info
        blissdata.newsample('abc')
        self.assertEqual(save_info.sample, 'abc')
        blissdata.newsample(None)
        self.assertEqual(save_info.sample, 'abc')

    def test_dataset(self):
        save_info = self.save_info
        kwargs = {'nonexisting': False}
        blissdata.newsample('abc')
        blissdata.newdataset(**kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, '0001')
        blissdata.newdataset(**kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, '0001')
        blissdata.newdataset('def', **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'def')
        blissdata.newdataset('def', **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'def')

        kwargs = {'nonexisting': True}
        blissdata.newsample('def')
        blissdata.newdataset(**kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, '0001')
        blissdata.newdataset(10, **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, '0010')
        blissdata.newdataset(2, **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, '0011')
        blissdata.newdataset('def', **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'def')
        blissdata.newdataset('def', **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'def_0001')
        blissdata.newdataset('def_0010', **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'def_0010')
        blissdata.newdataset('def', **kwargs)
        mkdir(save_info.get_path())
        self.assertEqual(save_info.dataset, 'def_0011')


def test_suite():
    """Test suite including all test suites"""
    testSuite = unittest.TestSuite()
    testSuite.addTest(test_blissdata("test_experiment"))
    testSuite.addTest(test_blissdata("test_sample"))
    testSuite.addTest(test_blissdata("test_dataset"))
    return testSuite


if __name__ == '__main__':
    import sys

    mysuite = test_suite()
    runner = unittest.TextTestRunner()
    if not runner.run(mysuite).wasSuccessful():
        sys.exit(1)
