"""
Sample catalogue in Redis
"""
from bliss.config import settings
from bliss.shell.cli.user_dialog import UserInput, Validator
from bliss.shell.cli.pt_widgets import BlissDialog
from ..utils import config
from . import compounds


DEFAULT = {'description': '', 'formula': ''}


def catalogue(name=None):
    """
    Sample catalogue of the current experiment

    :param str name: experiment name
    :returns ParametersWardrobe:
    """
    if not name:
        name = config.scan_saving_get('experiment')
        if not name:
            return None
        name = name.upper()
    cat = settings.ParametersWardrobe('SAMPLE_CATALOGUE_' + name,
                                      default_values=DEFAULT)
    return cat


def remove_catalogue(name=None):
    """
    Remove sample catalogue of an experiment

    :param str name: experiment name
    """
    cat = catalogue(name)
    if cat is not None:
        cat.purge()


def definition(name=None):
    """
    Get sample definition

    :param str name: sample name
    :returns ParametersWardrobe:
    """
    if not name:
        name = config.scan_saving_get('sample')
    if name:
        cat = catalogue()
        cat.switch(name)
        return cat
    else:
        return None


def _validator(name):
    """
    :param str name:
    :returns callable:
    """
    if name == 'formula':
        parser = compounds.FormulaParser()

        def asformula(formula):
            if not formula:
                return formula
            if not parser.validate(formula):
                raise ValueError('formula')
            return formula
        return Validator(asformula)
    else:
        return None


def edit_interactive(name=None):
    """
    Edit sample definition (interactive)
    """
    cat = catalogue()
    if not name:
        name = config.scan_saving_get('sample')
    if not name:
        return
    lst = []
    for k in DEFAULT:
        dlg = UserInput(label=k,
                        defval=getattr(cat, k),
                        validator=_validator(k))
        lst.append([dlg])
    results = BlissDialog(lst,
                          title='Sample {} definition'.format(repr(name)),
                          paddings=(1, 1)).show()
    if results:
        for dlg, v in results.items():
            k = dlg.label
            try:
                setattr(cat, k, v)
            except AttributeError:
                pass


def define(name=None, edit=True, **kwargs):
    """
    Define a sample

    :param str name: sample name
    :param bool edit: interactive editing by user
    :param **kwargs: sample attributes
    """
    cat = catalogue()
    if not name:
        name = config.scan_saving_get('sample')
    if not name:
        return
    cat.switch(name)
    for k, v in kwargs.items():
        if k not in DEFAULT:
            continue
        try:
            validator = _validator(k)
            if validator is not None:
                v = validator.check(v)
        except Exception:
            continue
        try:
            setattr(cat, k, v)
        except AttributeError:
            pass
    if edit:
        edit_interactive(name)
