import re
import pyparsing as pp


class FormulaParser(object):

    def __init__(self):
        lpar = pp.Literal("(").suppress()
        rpar = pp.Literal(")").suppress()

        element = pp.Combine(pp.Word(pp.srange("[A-Z]"), exact=1) +
                             pp.Optional(pp.Word(pp.srange("[a-z]"), max=1)))
        integer = pp.Word(pp.nums)
        point = pp.Literal(".")
        fnumber = pp.Combine(integer + pp.Optional(point+pp.Optional(integer))) |\
                  pp.Combine(point + integer)

        self.formula = pp.Forward()
        atom = element | pp.Group(lpar + self.formula+rpar)
        self.formula << pp.OneOrMore(
            pp.Group(atom + pp.Optional(fnumber, default="1")))
        self.elements = {}

    def parseresult(self, result, mult):
        """
        :param ParseResults or str result:
        :param float mult: multiplier
        """
        if isinstance(result, pp.ParseResults):
            if isinstance(result[-1], str):
                if not result[-1].isalpha():
                    mult *= float(result[-1])
            for r in result:
                self.parseresult(r, mult)
        elif result[-1].isalpha():
            if result in self.elements:
                self.elements[result] += mult
            else:
                self.elements[result] = mult
        else:
            pass

    def parse(self, formula):
        """
        :param str formula:
        :param dict(str:num): elemental composition
        """
        self.elements = {}
        try:
            result = self.formula.parseString(formula)
            self.parseresult(result, 1.)
        except pp.ParseBaseException:
            self.elements = {}
        return self.elements

    def validate(self, formula):
        elements = set(re.findall(r'[A-Z][a-z]?', formula))
        if not elements:
            return False
        return set(self.parse(formula).keys()) == elements
