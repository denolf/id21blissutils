"""
Manage Bliss data saving (SCAN_SAVING)
"""

import os
from datetime import datetime
from collections import OrderedDict
from nexus_writer_service.utils import data_policy 
from nexus_writer_service.utils.data_policy import newexperiment, newvisitor, newinhouse
from ..io import utils as io_utils
from ..utils import config
from . import naming


def assert_experiment(scan_saving):
    try:
        for attr in 'experiment', 'beamline':
            assert bool(getattr(scan_saving, attr))
    except (AttributeError, AssertionError):
        raise RuntimeError("Run 'newexperiment' first")


def assert_sample(scan_saving):
    assert_experiment(scan_saving)
    try:
        assert bool(getattr(scan_saving, 'sample'))
    except (AttributeError, AssertionError):
        raise RuntimeError("Run 'newsample' first")


def assert_dataset(scan_saving):
    assert_sample(scan_saving)
    try:
        for attr in 'dataset', 'technique':
            assert bool(getattr(scan_saving, attr))
    except (AttributeError, AssertionError):
        raise RuntimeError("Run 'newdataset' first")


def newsample(name=None):
    """
    Change sample in session's SCAN_SAVING object

    :param str name: keep current by default ('default' when missing)
    """
    scan_saving = config.scan_saving()
    assert_experiment(scan_saving)
    try:
        scan_saving.sample
    except AttributeError:
        if not name:
            name = 'default'
        data_policy.raise_invalid_characters(r"0-9a-zA-Z_\s\-", sample=name)
        scan_saving.add('sample', name)
    else:
        if name:
            data_policy.raise_invalid_characters(r"0-9a-zA-Z_\s\-", sample=name)
            scan_saving.sample = name
        elif not scan_saving.sample:
            scan_saving.sample = 'default'


def newdataset(name=None, nonexisting=False):
    """
    Change dataset in session's SCAN_SAVING object.
    When no name is specify, a new number starting from 1 will be used

    :param str or num name:
    :param bool nonexisting: nonexisting dataset directory
    """
    scan_saving = config.scan_saving()
    assert_sample(scan_saving)
    template = os.path.join('{base_path}', scan_saving.template)
    variables = config.scan_saving_pathinfo()
    name = naming.dataset_name(name, template, variables,
                               nonexisting=nonexisting)
    scan_saving.add('dataset', name)


def switchtechnique(name=None):
    """
    Change technique in session's SCAN_SAVING object

    :param str name:
    """
    scan_saving = config.scan_saving()
    assert_dataset(scan_saving)
    if not name:
        name = config.scan_saving_get('technique')
    if not name:
        name = config.default_technique()
    lst = config.techniques()
    if lst and name not in lst:
        raise ValueError('{} is not a valid technique. Available techniques are {}'.format(repr(name), lst))
    scan_saving.add('technique', name)


def saveinfo():
    """
    Bliss saving info

    :returns OrderedDict:
    """
    scan_saving = config.scan_saving()
    info = OrderedDict()
    try:
        scan_saving.get_path()
    except RuntimeError:
        info['status'] = 'INCOMPLETE'
    else:
        info['status'] = 'COMPLETE'
    info['path'] = scan_saving.get_path()

    attrs = ['template', 'experiment', 'base_path',
             'beamline', 'sample', 'dataset', 'technique']
    keymap = {'base_path': 'root', 'experiment': 'proposal'}
    for attr in attrs:
        try:
            info[keymap.get(attr, attr)] = getattr(scan_saving, attr, None)
        except RuntimeError:
            info[keymap.get(attr, attr)] = None
    return info
