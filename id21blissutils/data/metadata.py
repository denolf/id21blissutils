"""
Manage MetadataManager data saving
 https://gitlab.esrf.fr/icat/tango-metadata
"""
from collections import OrderedDict
from ..utils import config
from . import metasync


def enable(push=True, reset=None, validate=True):
    """
    Enable metadata and reset the MetadataManager

    :param bool push: synchronize Bliss and MetadataManager (push by default)
    :param str reset: reset the MetadataManager before sync ('hard', 'soft')
    """
    # TODO: add to metasync.attributes
    if metasync.enabled():
        return
    prx = metasync.metaproxy(allow_disabled=True)
    if not prx:
        return
    config.scan_saving_set('mdataon', bool(prx))
    try:
        if reset == 'soft' or reset == 'hard':
            metasync.reset(force=reset == 'hard')
        metasync.sync(push=push, validate=validate)
    except Exception:
        config.scan_saving_set('mdataon', False)
        raise


def disable(close=False, reset=None):
    """
    Disable metadata

    :param bool close: close dataset
    :param str reset: reset the MetadataManager after close ('hard', 'soft')
    """
    if close:
        enddataset()
    config.scan_saving_set('mdataon', False)
    if close:
        if reset == 'soft' or reset == 'hard':
            metasync.reset(force=reset == 'hard')


def saveinfo():
    """
    Dataset saving info
    """
    info = OrderedDict()
    prx = metasync.metaproxy()
    if not prx:
        return info
    info['status'] = prx.status
    info['path'] = prx.path
    info.update(metasync.attribute_dict(*metasync.saving_parameters(),
                                        other=True))
    return info


def startdataset():
    prx = metasync.metaproxy()
    if prx:
        prx.startDataset()
        metasync.pushmeta()


def enddataset():
    prx = metasync.metaproxy()
    if prx:
        if prx.state == 'RUNNING':
            metasync.pushmeta()
            prx.endDataset()


def elogmessage(*msg, allow_disabled=False, type=None):
    """
    Write in the electronic logbook
    """
    prx = metasync.metaproxy(allow_disabled=allow_disabled)
    if prx:
        client = config.scan_saving_get('session', None)
        prx.elogmessage(*msg, client=client, type=type)
