"""
Synchronize Bliss and MetadataManager
"""
import logging
import os
import re
from contextlib import contextmanager
from collections import OrderedDict
from . import sample_catalogue
from ..utils import config
from ..client import metadata


logger = logging.getLogger(__name__)


class SetParameterError(RuntimeError):
    pass


class GetParameterError(RuntimeError):
    pass


def enabled():
    """
    Is the Metadata enabled in the Bliss session.
    """
    return config.scan_saving_get('mdataon', False)


def disabled():
    """
    Is the Metadata disabled in the Bliss session.
    """
    return not enabled()


def handle_error(msg, raise_on_error=True, silent=False, **kwargs):
    if not silent:
        logger.error(msg)
    if raise_on_error:
        raise RuntimeError(msg)


def metaproxy(allow_disabled=False, **kwargs):
    """
    Generates a metadata proxy

    :returns MetadataClient or None:
    """
    if disabled() and not allow_disabled:
        return None
    mdata = config.nexus_definitions().get('mdata', {})
    uri = mdata.get(config.current_technique(), None)
    if not uri:
        uri = mdata.get('default', None)
    if uri:
        prx = metadata.MetadataClient(uri)
        if not prx.online:
            handle_error('MetadataManager is OFFLINE', **kwargs)
        return prx
    else:
        handle_error('URI for MetadataManager not specified in beamline configuration')
        return None


class Parameter(object):
    """
    Parameter with synchronized equivalent
    """

    def __init__(self, name, validated=None, unvalidated=None):
        """
        :param str name:
        :param Parameter validated: synchronized equivalent
        :param Parameter unvalidated: without validation
        """
        self.name = name
        self.validated = validated
        if unvalidated is None:
            unvalidated = validated
        self.unvalidated = unvalidated

    def other(self, validate=False):
        if validate:
            return self.validated
        else:
            return self.unvalidated

    def push(self, validate=True, **kwargs):
        other = self.other(validate=validate)
        if other is not None:
            other.set_value(self.get_value(**kwargs), **kwargs)

    def pull(self, validate=True, **kwargs):
        other = self.other(validate=validate)
        if other is not None:
            self.set_value(other.get_value(**kwargs), **kwargs)

    def sync(self, push=True, **kwargs):
        if push:
            self.push(**kwargs)
        else:
            self.pull(**kwargs)

    def get_other_value(self, **kwargs):
        return self.other(validate=False).get_value(**kwargs)

    def synchronized(self, **kwargs):
        return self.get_value(**kwargs) == self.get_other_value(**kwargs)

    def __hash__(self):
        return hash(self.name)

    def __repr__(self):
        return repr(self.name)

    def __str__(self):
        return str(self.name)

    def __eq__(self, other):
        return self.name == other

    def proxy(self, **kwargs):
        raise NotImplementedError

    def get_value(self, **kwargs):
        raise NotImplementedError

    def set_value(self, value, **kwargs):
        raise NotImplementedError

    def exists(self, **kwargs):
        raise NotImplementedError

    def create(self, default, **kwargs):
        if self.exists(**kwargs) is False:
            self.set_value(default, **kwargs)


class AttributeParameter(Parameter):
    """
    Parameter is an attribute of its proxy
    """

    def get_value(self, **kwargs):
        prx = self.proxy(**kwargs)
        if prx:
            try:
                return getattr(prx, self.name)
            except AttributeError as e:
                raise GetParameterError(str(e))
        return None

    def set_value(self, value, **kwargs):
        prx = self.proxy(**kwargs)
        if prx:
            try:
                setattr(prx, self.name, value)
            except AttributeError as e:
                raise SetParameterError(str(e))

    def exists(self, **kwargs):
        prx = self.proxy(**kwargs)
        if prx:
            return hasattr(prx, self.name)
        return None


class ItemParameter(Parameter):
    """
    Parameter is an item of its proxy
    """

    def get_value(self, **kwargs):
        prx = self.proxy(**kwargs)
        if prx:
            try:
                return prx[self.name]
            except KeyError as e:
                raise GetParameterError(str(e))
        return None

    def set_value(self, value, **kwargs):
        prx = self.proxy(**kwargs)
        if prx:
            try:
                prx[self.name] = value
            except KeyError as e:
                raise SetParameterError(str(e))

    def exists(self, **kwargs):
        prx = self.proxy(**kwargs)
        if prx:
            return self.name in prx
        return None


class ScanSavingAttribute(AttributeParameter):
    """
    Parameter is an attribute of SCAN_SAVING
    """

    def proxy(self, allow_disabled=False, **kwargs):
        if disabled() and not allow_disabled:
            return None
        else:
            return config.scan_saving()

    def create(self, default, **kwargs):
        if not self.exists(**kwargs):
            prx = self.proxy(**kwargs)
            if prx:
                prx.add(self.name, default)


class ScanSavingTemplate(ScanSavingAttribute):
    """
    Parameter is an attribute of SCAN_SAVING
    """

    def set_value(self, template, **kwargs):
        # template does not contain root
        if template:
            prx = self.proxy(**kwargs)
            if prx:
                for attr in re.findall(r'\{(.*?)\}', template):
                    if not hasattr(prx, attr):
                        prx.add(attr, '')
        super().set_value(template, **kwargs)


class SampleAttribute(AttributeParameter):
    """
    Parameter is an attribute of the sample definition
    """

    def proxy(self, allow_disabled=False, **kwargs):
        if disabled() and not allow_disabled:
            return None
        else:
            return sample_catalogue.definition()


class MetadataManagerAttribute(AttributeParameter):
    """
    Parameter is an attribute of the MetadataManager
    """

    def proxy(self, **kwargs):
        return metaproxy(**kwargs)

    @contextmanager
    def _get_errorhandling(self, **kwargs):
        try:
            yield
        except metadata.StateException as e:
            handle_error('Getting {} failed because:\n{}'
                         .format(repr(self.name), e),
                         **kwargs)

    @contextmanager
    def _set_errorhandling(self, value, **kwargs):
        try:
            yield
        except metadata.StateException as e:
            handle_error('Setting {} to {} failed because:\n{}'
                         .format(repr(self.name), repr(value), e),
                         **kwargs)
        except SetParameterError:
            if value != self.get_value(**kwargs):
                msg = "Set {} = {} in {} and restart it"\
                    .format(repr(self.name), repr(value), self.proxy(**kwargs))
                handle_error(msg, **kwargs)

    def get_value(self, **kwargs):
        with self._get_errorhandling(**kwargs):
            return super().get_value(**kwargs)

    def set_value(self, value, **kwargs):
        with self._set_errorhandling(value, **kwargs):
            super().set_value(value, **kwargs)


class MetadataManagerTemplate(MetadataManagerAttribute):
    """
    'template' attribute of the MetadataManager
    """

    # These are the allowed template parameters:
    bliss_to_meta = {'base_path': 'dataRoot',
                     'experiment': 'proposal',
                     'beamline': 'beamlineID',
                     'sample': 'sampleName',
                     'dataset': 'scanName'}

    set_map = {k: '{' + v + '}' for k, v in bliss_to_meta.items()}
    get_map = {k: '{' + v + '}' for v, k in bliss_to_meta.items()}

    def set_value(self, template, **kwargs):
        if template:
            # MetadataManager's dataset name includes the sample as prefix
            template = template.replace('{sample}_{dataset}', '{dataset}')
            # template does not contain root
            template = os.path.join('{dataRoot}', template.format(**self.set_map))
        super().set_value(template, **kwargs)

    def get_value(self, **kwargs):
        template = super().get_value(**kwargs)
        if template:
            # MetadataManager's dataset name includes the sample as prefix
            template = template.replace('{scanName}', '{sampleName}_{scanName}')
            # template should not contain root
            template = os.path.relpath(template.format(**self.get_map), '{base_path}')
        return template


class IcatAttribute(MetadataManagerAttribute):
    """
    Icat attribute of the MetadataManager
    """

    def get_value(self, **kwargs):
        prx = self.proxy(**kwargs)
        if prx:
            with self._get_errorhandling(**kwargs):
                return prx.get_icat_attribute(self.name)
        return None

    def set_value(self, value, **kwargs):
        prx = self.proxy(**kwargs)
        if prx:
            with self._set_errorhandling(value, **kwargs):
                prx.set_icat_attribute(self.name, value)


# Order is important for pushing
# TODO: MotorListAttribute
attributes = [
    ScanSavingTemplate('template', MetadataManagerTemplate('template')),
    ScanSavingAttribute('experiment', MetadataManagerAttribute('proposal')),
    ScanSavingAttribute('base_path', MetadataManagerAttribute('root')),
    ScanSavingAttribute('beamline', MetadataManagerAttribute('beamline')),
    ScanSavingAttribute('sample', MetadataManagerAttribute('sample')),
    ScanSavingAttribute('dataset', MetadataManagerAttribute('dataset')),
    ScanSavingAttribute('technique', MetadataManagerAttribute('technique')),
    SampleAttribute('description', IcatAttribute('Sample_description')),
    SampleAttribute('formula', IcatAttribute('Sample_chemical_formula')),
]


def iter_attributes(*parameters, exclude=False, **kwargs):
    """
    Iterator over attributes with filtering
    """
    if exclude:
        for p in attributes:
            if p not in parameters:
                yield p
    else:
        if parameters:
            for p in parameters:
                yield attributes[attributes.index(p)]
        else:
            for p in attributes:
                yield p


def sync(*parameters, **kwargs):
    """
    Synchronize Bliss and MetadataManager
    """
    for p in iter_attributes(*parameters, **kwargs):
        p.sync(**kwargs)


def push(*parameters, **kwargs):
    """
    Synchronize Bliss and MetadataManager
    """
    sync(*parameters, push=True, **kwargs)


def pull(*parameters, **kwargs):
    """
    Synchronize Bliss and MetadataManager
    """
    sync(*parameters, push=False, **kwargs)


def pushpull(*parameters, **kwargs):
    """
    Synchronize Bliss and MetadataManager
    """
    push(*parameters, **kwargs)
    pull(*parameters, **kwargs)


def reset(force=False, **kwargs):
    """
    Reset the MetadataManager (proposal, sample and dataset)

    :param bool force:
    """
    prx = metaproxy(**kwargs)
    if not prx:
        return
    try:
        prx.reset(force=force)
    except metadata.StateException as e:
        handle_error('Cannot reset {}:\n{}'.format(metaproxy, e))


def attribute_dict(*parameters, other=False, **kwargs):
    """
    :returns OrderedDict:
    """
    ret = OrderedDict()
    for p in iter_attributes(*parameters, **kwargs):
        if other:
            p = p.other(validate=False)
        ret[p.name] = p.get_value(**kwargs)
    return ret


def synchronized(*parameters, **kwargs):
    """
    :returns bool:
    """
    kwargs['raise_on_error'] = False
    return all(p.synchronized(**kwargs)
               for p in iter_attributes(*parameters))


def assertSynchronized(*parameters, **kwargs):
    """
    :raises RuntimeError: when MetadataManager is enabled but not synchronized
    """
    if enabled():
        if not synchronized(*parameters, **kwargs):
            raise RuntimeError('{} not synchronized with MetadataManager'
                               .format(parameters))


def saving_parameters():
    """
    All parameters associated to scan saving
    """
    return [p.name for p in attributes
            if isinstance(p, ScanSavingAttribute)]


def pushmeta():
    """
    Push all metadata except for the scan saving parameters
    """
    push(*saving_parameters(), exclude=True)
