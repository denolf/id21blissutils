import unittest
import gevent
import numpy
from nexus_writer_service.session_writer import default_saveoptions
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan
from ...testing.unittest import BlissTestCase
from ...utils import cli

logger = cli.getLogger(__name__, __file__)


class test_scan_writer(BlissTestCase):

    def setUp(self, **saveoptions):
        self.repeats = 1
        self.npoints1d = 10
        self.npoints2d = 3, 4
        self.expotime = 0.1
        self.nasync = 10
        super(test_scan_writer, self).setUp(writer='server',
                                            xrf=True,
                                            xrd=True,
                                            managed=True,
                                            keepfiles=False,
                                            saveoptions=saveoptions)

    def test_ct(self):
        env = self.session_env
        detectors = self.detectors

        scans = []
        for _ in range(self.repeats):
            scan = env.ct(self.expotime, *detectors, save=True, run=False)
            self._run_scan(scan)
            scans.append(scan)
        self._wait_scans_finished(scans)
        self._opendata()

        for scan in scans:
            with self._assert_output(scan, scanshape=tuple()) as entry:
                self._backup(entry)

    def test_loopscan(self):
        env = self.session_env
        detectors = self.detectors

        scans = []
        for _ in range(self.repeats):
            scan = env.loopscan(self.npoints1d, self.expotime, *detectors, run=False)
            self._run_scan(scan)
            scans.append(scan)
        self._wait_scans_finished(scans)
        self._opendata()

        for scan in scans:
            with self._assert_output(scan, scanshape=(self.npoints1d,)) as entry:
                self._backup(entry)

    @unittest.skip('for now')
    def test_timescan(self):
        env = self.session_env
        detectors = self.detectors

        scans = []
        for _ in range(self.repeats):
            scan = env.timescan(self.expotime, *detectors, run=False)
            greenlet = self._run_scan(scan, runasync=True)
            gevent.sleep(self.npoints1d * self.expotime + 10)
            greenlet.kill()
            greenlet.join()
            scans.append(scan)
        self._wait_scans_finished(scans)
        self._opendata()

        for scan in scans:
            with self._assert_output(scan, scanshape=None) as entry:
                self._backup(entry)

    def test_ascan(self):
        env = self.session_env
        mot1 = env.simpx
        detectors = self.detectors

        scans = []
        for _ in range(self.repeats):
            env.mv(mot1, 3)
            scan = env.ascan(mot1, 0, 10, self.npoints1d-1, self.expotime, *detectors, run=False)
            self._run_scan(scan)
            scans.append(scan)
        self._wait_scans_finished(scans)
        self._opendata()

        for scan in scans:
            with self._assert_output(scan, scanshape=(self.npoints1d,), masters=[mot1.name]) as entry:
                instrument = entry['instrument']
                self.assertEqual(instrument['positioners_start']['simpx'][()],
                                 3, msg=self._assertmsg(entry))
                if not self.simplewriter:
                    self.assertEqual(instrument['positioners_end']['simpx'][()],
                                     10, msg=self._assertmsg(entry))
                self._backup(entry)

    @unittest.skip('bad triggering')
    def test_zapline(self):
        env = self.session_env
        mot1 = env.simpx
        detectors = self.detectors

        scans = []
        for _ in range(self.repeats):
            env.mv(mot1, 3)
            scan = env.zapline(mot1, 0, 10, self.npoints1d, self.expotime, *detectors, run=False)
            self._run_scan(scan)
            scans.append(scan)
        self._wait_scans_finished(scans)
        self._opendata()

        for scan in scans:
            with self._assert_output(scan, scanshape=(self.npoints1d,),
                masters=[mot1.name], hastimer=False) as entry:
                instrument = entry['instrument']
                self.assertEqual(instrument['positioners_start']['simpx'][()],
                                 3, msg=self._assertmsg(entry))
                if not self.simplewriter:
                    self.assertEqual(instrument['positioners_end']['simpx'][()],
                                     10, msg=self._assertmsg(entry))
                self._backup(entry)

    def test_amesh(self):
        env = self.session_env
        mot1, mot2 = env.simpx, env.simpy
        detectors = self.detectors

        n1, n2 = self.npoints2d
        scans = []
        for _ in range(self.repeats):
            env.mv(mot1, 3)
            env.mv(mot2, 4)
            scan = env.amesh(mot1, 0, 4, n1-1, mot2, 0, -5, n2-1, self.expotime, *detectors, run=False)
            self._run_scan(scan)
            scans.append(scan)
        self._wait_scans_finished(scans)
        self._opendata()

        for scan in scans:
            with self._assert_output(scan, scanshape=(n1, n2), masters=['simpx', 'simpy']) as entry:
                instrument = entry['instrument']
                self.assertEqual(instrument['positioners_start']['simpx'][()],
                                 3, msg=self._assertmsg(entry))
                self.assertEqual(instrument['positioners_start']['simpy'][()],
                                 4, msg=self._assertmsg(entry))
                if not self.simplewriter:
                    self.assertEqual(instrument['positioners_end']['simpx'][()],
                                     4, msg=self._assertmsg(entry))
                    self.assertEqual(instrument['positioners_end']['simpy'][()],
                                     -5, msg=self._assertmsg(entry))
                self._backup(entry)

    def _test_infinite(self):
        tests = (self.test_ct, self.test_loopscan,
                 self.test_timescan, self.test_ascan,
                 self.test_amesh)
        while True:
            for test in tests:
                test()
            break

    @unittest.skip('data size of subscans are not always correct')
    def test_subscan(self):
        if not self.saveoptions['flat']:
            self.skipTest('Subscans do not publish their shape in Redis')
        env = self.session_env
        detectors = self.detectors
        mot1, mot2 = env.simpx, env.simpy
        chain = AcquisitionChain()
        s1 = env.loopscan(self.npoints, self.expotime, *detectors, run=False)
        n1, n2 = self.npoints2d
        s2 = env.amesh(mot1, 0, 4, n1-1, mot2, 0, -3, n2-1, self.expotime, *detectors, run=False)
        chain.append(s1.acq_chain)
        chain.nodes_list[0].terminator = False
        chain.append(s2.acq_chain)
        scan = Scan(chain, "test")
        self._run_scan(scan)
        self._wait_scans_finished([scan])
        self._opendata()

        with self._assert_output(scan, scanshape=(n1, n2),
                                 masters=['simpx', 'simpy'],
                                 subscan=1) as entry:
            pass
        return
        with self._assert_output(scan, scanshape=(self.npoints, ),
                                 subscan=2) as entry:
            pass

    @unittest.skip('data size of subscans are not always correct')
    def test_async_timescan(self):
        env = self.session_env
        detectors = self.detectors
        env.mv(env.simpx, 3)

        scans = []
        for _ in range(self.nasync):
            scans.append(env.timescan(self.expotime, *detectors,
                                      wait=False, run=False))
        greenlets = [self._run_scan(s, runasync=True) for s in scans]
        gevent.sleep(10)
        gevent.killall(greenlets)
        gevent.joinall(greenlets)
        self._wait_scans_finished(scans)
        self._opendata()

        for scan in scans:
            with self._assert_output(scan, scanshape=None) as entry:
                instrument = entry['instrument']
                self.assertEqual(instrument['positioners_start']['simpx'][()],
                                 3, msg=self._assertmsg(entry))
                if not self.simplewriter:
                    self.assertEqual(instrument['positioners_end']['simpx'][()],
                                     3, msg=self._assertmsg(entry))

    @unittest.skip('data size of subscans are not always correct')
    def test_async_ct(self):
        env = self.session_env
        detectors = self.detectors

        env.mv(env.simpx, 3)
        scans = []
        for _ in range(self.nasync):
            scans.append(env.ct(self.expotime, *detectors,
                         save=True, wait=False, run=False))
        greenlets = [self._run_scan(s, runasync=True) for s in scans]
        gevent.joinall(greenlets)
        self._wait_scans_finished(scans)
        self._opendata()

        for scan in scans:
            with self._assert_output(scan, scanshape=tuple()) as entry:
                instrument = entry['instrument']
                self.assertEqual(instrument['positioners_start']['simpx'][()],
                                 3, msg=self._assertmsg(entry))
                if not self.simplewriter:
                    self.assertEqual(instrument['positioners_end']['simpx'][()],
                                     3, msg=self._assertmsg(entry))

    @unittest.skip('data size of subscans are not always correct')
    def test_async_loopscan(self):
        env = self.session_env
        detectors = self.detectors
        env.mv(env.simpx, 3)

        scans = []
        shapes = []
        for n in numpy.random.randint(10, 20, self.nasync):
            scans.append(env.loopscan(int(n), self.expotime, *detectors,
                                      wait=False, run=False))
            shapes.append((n,))
        greenlets = [self._run_scan(s, runasync=True) for s in scans]
        gevent.joinall(greenlets)
        self._wait_scans_finished(scans)
        self._opendata()

        for scan, shape in zip(scans, shapes):
            with self._assert_output(scan, scanshape=shape) as entry:
                instrument = entry['instrument']
                self.assertEqual(instrument['positioners_start']['simpx'][()],
                                 3, msg=self._assertmsg(entry))
                if not self.simplewriter:
                    self.assertEqual(instrument['positioners_end']['simpx'][()],
                                     3, msg=self._assertmsg(entry))

    @unittest.skip('data size of subscans are not always correct')
    def test_async_random(self):
        env = self.session_env
        motors = [env.simpx, env.simpy, env.simpz,
                  env.simx, env.simy, env.simz]
        detectors = self.detectors

        finitescans = []
        infinitescans = []
        kwargs = []
        for _ in range(10):
            nmotors = len(motors)
            if nmotors == 0:
                choices = ['ct', 'loopscan', 'timescan']
            elif nmotors == 1:
                choices = ['ascan']
            else:
                choices = ['ascan', 'amesh']
            scantype = numpy.random.choice(choices)
            if scantype == 'ct':
                scan = env.ct(self.expotime, *detectors, save=True,
                              wait=False, run=False)
                kwargs.append({'scanshape': tuple()})
                finitescans.append(scan)
            elif scantype == 'loopscan':
                scan = env.loopscan(self.npoints1d, self.expotime, *detectors,
                                    wait=False, run=False)
                kwargs.append({'scanshape': (self.npoints1d,)})
                finitescans.append(scan)
            elif scantype == 'ascan':
                mot1 = motors.pop()
                scan = env.ascan(mot1, 0, 10, self.npoints1d-1, self.expotime, *detectors,
                                 wait=False, run=False)
                kwargs.append({'scanshape': (self.npoints1d,), 'masters': [mot1.name]})
                finitescans.append(scan)
            elif scantype == 'amesh':
                mot1 = motors.pop()
                mot2 = motors.pop()
                scan = env.amesh(mot1, 0, 4, self.npoints2d[0]-1,
                                 mot2, 0, -3, self.npoints2d[1]-1, self.expotime,
                                 *detectors, wait=False, run=False)
                kwargs.append({'scanshape': self.npoints2d, 'masters': [mot1.name, mot2.name]})
                finitescans.append(scan)
            elif scantype == 'timescan':
                scan = env.timescan(self.expotime, *detectors,
                                    wait=False, run=False)
                infinitescans.append(scan)
            else:
                continue

        # Start scans
        if finitescans:
            finitegreenlets = [self._run_scan(s, runasync=True) for s in finitescans]
        if infinitescans:
            infinitegreenlets = [self._run_scan(s, runasync=True) for s in infinitescans]

        # Wait for finite scans to finish
        if finitescans:
            gevent.joinall(finitegreenlets)
            self._wait_scans_finished(finitescans)

        # Stop infinite scans
        if infinitescans:
            gevent.sleep(10)
            gevent.killall(infinitegreenlets)
            gevent.joinall(infinitegreenlets)
            self._wait_scans_finished(finitescans)

        self._opendata()
        scans = finitescans + infinitescans
        kwargs += [{'scanshape': tuple()}] * len(infinitescans)
        for scan, kwarg in zip(scans, kwargs):
            with self._assert_output(scan, **kwarg) as entry:
                self._backup(entry)

    def _run_scan(self, scan, runasync=False):
        for node in scan.acq_chain.nodes_list:
            if 'frelon' in node.name:
                sim_params = node.parameters
                sim_params["saving_format"] = "HDF5"
                sim_params["saving_frame_per_file"] = 3
                sim_params["saving_suffix"] = ".h5"
        if runasync:
            return gevent.spawn(scan.run)
        else:
            return scan.run()

    def _opendata(self):
        return
        self.session_env.opendata(block=True, config_writer=not self.simplewriter)

    def _backup(self, entry):
        return
        import os
        if not os.path.isdir('/data/id21/inhouse/wout/bliss/nexus/'):
            return
        from shutil import copyfile
        from datetime import datetime
        #cmd = entry['title'][()].split(' ')[0]
        dst = '/data/id21/inhouse/wout/bliss/nexus/{}_{}.h5'\
              .format(self.__class__.__name__, datetime.now().strftime('%d%b%Y'))
        copyfile(entry.file.filename, dst)


class test_scan_writer_other_options(test_scan_writer):

    def setUp(self, **saveoptions):
        # Opposite of the default options
        saveoptions.update({k: not v for k, v in default_saveoptions().items()})
        saveoptions['allow_external_hdf5'] = True
        super(test_scan_writer_other_options, self).setUp(**saveoptions)


class test_scan_writer_simple(test_scan_writer):

    def setUp(self):
        super(test_scan_writer_simple, self).setUp(noconfig=True)


class test_scan_writer_simple_other_options(test_scan_writer_other_options):

    def setUp(self):
        super(test_scan_writer_simple_other_options, self).setUp(noconfig=True)


def test_suite():
    """Test suite including all test suites"""
    testSuite = unittest.TestSuite()
    #testSuite.addTest(test_scan_writer("test_zapline"))
    #return testSuite
    #testSuite.addTest(test_scan_writer("_test_infinite"))
    #testSuite.addTest(test_scan_writer_simple("_test_infinite"))
    #return testSuite
    utclasses = (test_scan_writer, test_scan_writer_other_options,
                 test_scan_writer_simple, test_scan_writer_simple_other_options)
    for utclass in utclasses:
        testSuite.addTest(utclass("test_ct"))
        testSuite.addTest(utclass("test_loopscan"))
        testSuite.addTest(utclass("test_timescan"))
        testSuite.addTest(utclass("test_ascan"))
        testSuite.addTest(utclass("test_amesh"))
        testSuite.addTest(utclass("test_subscan"))
        testSuite.addTest(utclass("test_async_ct"))
        testSuite.addTest(utclass("test_async_timescan"))
        testSuite.addTest(utclass("test_async_loopscan"))
        testSuite.addTest(utclass("test_async_random"))
    return testSuite


if __name__ == '__main__':
    import sys

    mysuite = test_suite()
    runner = unittest.TextTestRunner()
    if not runner.run(mysuite).wasSuccessful():
        sys.exit(1)
