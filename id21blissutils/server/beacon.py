"""
Wrapper to launch a Bliss Beacon server
"""
import os
import logging
from .server import Server
from ..utils import socket
from ..io import utils as ioutils

logger = logging.getLogger(__name__)


# https://gitlab.esrf.fr/bliss/bliss/blob/31b14e40/bliss/config/conductor/server.py#L747
level_to_cli = {logging.CRITICAL: 'ERROR',
                logging.ERROR: 'ERROR',
                logging.WARNING: 'WARN',
                logging.INFO: 'INFO',
                logging.DEBUG: 'DEBUG'}


# https://github.com/tango-controls/pytango/blob/492c8752/tango/databaseds/database.py#L1745
level_to_tangocli = {logging.CRITICAL: 0,
                     logging.ERROR: 0,
                     logging.WARNING: 0,
                     logging.INFO: 1,
                     logging.DEBUG: 2}


class Beacon(Server):
    """
    Run Bliss Beacon server as a process
    """

    def __init__(self, **options):
        super(Beacon, self).__init__('beacon-server', **options)
        self.kwargmap = {'beacon_port': 'port'}

    def _kwargs_validate(self):
        if 'db_path' not in self._kwargs:
            raise ValueError("Specify configuration path 'db_path'")
        self._set_port('beacon_port', default=True)
        self._set_port('tango_port', default=True)
        self._set_port('redis_port', default=True)
        self._set_port('webapp_port', default=False)
        if 'redis_socket' not in self._kwargs:
            path = ioutils.temproot()
            sockname = ioutils.tempname(prefix='redis_', suffix='.sock')
            self._kwargs['redis_socket'] = os.path.join(path, sockname)
        if 'log_level' not in self._kwargs:
            level = logger.getEffectiveLevel()
            self._kwargs['log_level'] = level_to_cli.get(level, 'WARN')
        if 'tango_debug_level' not in self._kwargs:
            level = logger.getEffectiveLevel()
            self._kwargs['tango_debug_level'] = level_to_tangocli.get(level, 0)

    def _set_port(self, name, default=True):
        port = self.kwargs.get(name, default)
        if isinstance(port, bool):
            if port:
                port = socket.find_free_port()
            else:
                port = 0
        self.kwargs[name] = port

    def _wait(self, start=True, timeout=None):
        ports = 'beacon_port', 'tango_port', 'redis_port', 'webapp_port'
        ok = True
        for name in ports:
            port = self.kwargs[name]
            if port:
                ok &= socket.wait_port(port, open=start, timeout=timeout,
                                       msg=' ({})'.format(name), force=True)
        return ok

    @property
    def _env_generator(self):
        host = socket.hostname()
        ports = 'beacon_port', 'tango_port',
        env_vars = 'BEACON_HOST', 'TANGO_HOST'
        for name, env_var in zip(ports, env_vars):
            port = self.kwargs[name]
            if not port:
                continue
            value = "{}:{}".format(host, port)
            yield env_var, value

    @property
    def blissclient(self):
        lst = []
        for k, v in self._env_generator:
            lst.append('{}={}'.format(k, v))
        return lst

    @property
    def tangoclient(self):
        v = dict(self._env_generator)['TANGO_HOST']
        return ["TANGO_HOST={}".format(v)]

    @property
    def webclient(self):
        host = socket.hostname()
        port = self.kwargs['webapp_port']
        if port:
            return ["{}:{}".format(host, port)]
        else:
            return []

    @property
    def redisclient(self):
        return [self.kwargs['redis_socket']]
