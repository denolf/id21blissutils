"""
Wrapper to launch a Nexus writer
"""

import logging
import sys
from . import server


logger = logging.getLogger(__name__)


level_to_cli = {logging.CRITICAL: 'ERROR',
                logging.ERROR: 'ERROR',
                logging.WARNING: 'WARNING',
                logging.INFO: 'INFO',
                logging.DEBUG: 'DEBUG'}


class ScanWriter(server.Server):

    def __init__(self, session_name, **options):
        super(ScanWriter, self).__init__('NexusSessionWriter',
                                         session_name,
                                         **options)

    def _kwargs_validate(self):
        if 'log' not in self._kwargs:
            level = logger.getEffectiveLevel()
            self._kwargs['log'] = level_to_cli.get(level, 'WARNING')

    def _wait(self, start=True, timeout=None):
        return self.isrunning == start

    @property
    def _env_generator(self):
        return
        yield


class AsyncScanWriter(ScanWriter):

    def __init__(self, session_name):
        server.Server.__init__(self, session_name)
        self._async_func = session_writer.main

    def _kwargs_validate(self):
        self._kwargs = {}
