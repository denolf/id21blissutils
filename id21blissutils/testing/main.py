"""
CLI to create a Bliss test environment
"""
import sys
import argparse
import gevent
import gevent.select
from .environment import BlissTestEnv


def ginput(prompt):
    sys.stdout.write(prompt)
    sys.stdout.flush()
    gevent.select.select([sys.stdin], [], [])
    return sys.stdin.readline()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Run Bliss Test Environment (files + servers)')
    parser.add_argument('--blissenv', action='store_true',
                        help="Use configuration from bliss test suite")
    parser.add_argument('--path', default='', type=str,
                        help="Configuration path")
    parser.add_argument('--session', default='', type=str,
                        help="Session name")
    parser.add_argument('--webapp', action='store_true',
                        help="Start the beacon web server")
    parser.add_argument('--keepfiles', action='store_true',
                        help="Keep YAML files after exit")
    parser.add_argument('--xrf', action='store_true',
                        help="Add XRF devices")
    parser.add_argument('--xrd', action='store_true',
                        help="Add XRD devices")
    parser.add_argument('--fullfield', action='store_true',
                        help="Add Fullfield devices")
    parser.add_argument('--temperature', action='store_true',
                        help="Add Temperature devices")
    args, unknown = parser.parse_known_args()
    with BlissTestEnv(blissenv=args.blissenv,
                      session_name=args.session,
                      db_path=args.path,
                      webapp=args.webapp,
                      keepfiles=args.keepfiles,
                      xrf=args.xrf,
                      xrd=args.xrd,
                      fullfield=args.fullfield,
                      temperature=args.temperature) as testenv:
        print('Bliss Test Environment is setup in ' +
              repr(testenv._beacon.kwargs['db_path']))
        print('Connect to Bliss session like this:\n\n {}\n'
              .format(testenv.blissclient))
        print('Start Nexus writer like this:\n\n {}\n'
              .format(testenv.writerserver))
        print('Connect to Redis like this:\n\n {}\n'
              .format(testenv.redisclient))
        print('Connect to Tango database like this:\n\n {}\n'
              .format(testenv.tangoclient))
        s = testenv.webclient
        if s:
            print('Connect to Beacon webapp like this:\n\n {}\n'.format(s))
        # SIGINT gets passed to subprocess. This may cause
        # the Beacon servers to shut down before closing the Bliss session.
        #print('Press <CTRL-C> to stop the servers')
        #gevent.wait()
        # So use this solution instead
        p = gevent.spawn(ginput, 'Press any key to stop the servers')
        p.join()
        print('Bliss test environment exits.')
