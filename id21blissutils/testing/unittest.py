"""
Add Bliss Test Environment to unittest TestCase
"""
import unittest
import logging
import gevent
import numpy
import os
from nexus_writer_service import session_writer
from nexus_writer_service.io import nexus
from testfixtures import TempDirectory
from contextlib import contextmanager
from .environment import BlissTestEnv
from ..server import scanwriter
from ..utils import scanutils


logger = logging.getLogger(__name__)


class BlissTestCase(unittest.TestCase):
    """
    Setup Bliss test environment for unit tests
    """

    def setUp(self, writer=False, saveoptions=None, managed=False, **kwargs):
        kwargs['fullfield'] = False
        self.testenv = BlissTestEnv(**kwargs)
        self.testenv.setUp()
        self.dir = TempDirectory()
        self.writer = None
        if not saveoptions:
            saveoptions = {}
        self.saveoptions = saveoptions
        self._setup_writer(writer)
        if self.simplewriter:
            logger.warning('Only testing the basic writing (without plots and apps)')
        env = self.session_env
        self.managed = managed
        if self.managed:
            env.newlocalexperiment(technique='xrfxrd',
                                   root=self.dir.path)
        else:
            env.SCAN_SAVING.add('technique', 'xrfxrd')
            env.SCAN_SAVING.base_path = self.dir.path

    def tearDown(self):
        self._teardown_writer()
        self.testenv.tearDown()
        if not self.testenv.keepfiles:
            self.dir.cleanup()

    @property
    def simplewriter(self):
        return self.saveoptions.get('noconfig', False)

    def _setup_writer(self, writer):
        if not writer:
            return
        session_name = self.session_env.session_name
        if writer == 'local':
            # Spawn greenlet directly
            with self.cleanupContext():
                self.writer = session_writer.main(session_name,
                                                  **self.saveoptions)
        else:
            kwargs = {}
            cli_saveoptions = session_writer.all_cli_saveoptions(noconfig=self.simplewriter)
            for attr, (okwargs, option) in cli_saveoptions.items():
                if option not in self.saveoptions:
                    continue
                if okwargs['action'] == 'store_false':
                    kwargs[attr] = not self.saveoptions[option]
                else:
                    kwargs[attr] = self.saveoptions[option]
            # Use the server wrapper
            self.writer = scanwriter.ScanWriter(session_name,
                                                **kwargs)
            #self.writer = scanwriter.AsyncScanWriter(session_name,
            #                                         **kwargs)
            with self.cleanupContext():
                self.writer.start()
        for k, v in session_writer.default_saveoptions(noconfig=self.simplewriter).items():
            self.saveoptions[k] = self.saveoptions.get(k, v)
        # TODO: writer tango server to query online
        logger.info('Wait until writer is online ...')
        gevent.sleep(5)
        logger.info('Writer is assumed to be online')

    def _teardown_writer(self):
        if self.writer is None:
            return
        try:
            self.writer.stop()  # scanwriter.ScanWriter
        except AttributeError:
            self.writer.kill()  # Greenlet
            self.writer.join()

    @property
    def session_env(self):
        return self.testenv.session_env

    @contextmanager
    def cleanupContext(self):
        with self.testenv.cleanupContext():
            yield

    def _wait_scans_finished(self, scans, timeout=60):
        uris = [self.uris(scan, 1) for scan in scans]
        logger.debug('Wait for {} to finish writing ...'.format(uris))

        class TimeOutError(Exception):
            pass

        try:
            with gevent.Timeout(timeout, TimeOutError):
                while uris:
                    uris = [uri for uri in uris
                            if not nexus.nxComplete(uri)]
                    logger.debug('Wait for {} to finish writing ...'.format(uris))
                    gevent.sleep(1)
        except TimeOutError:
            if uris:
                self.testenv.keepfiles = True
            self.assertFalse(uris, 'Datasets not finished')

    @property
    def detectors(self):
        env = self.session_env
        if self.testenv.xrf and self.testenv.xrd:
            return env.simxrfxrdgroup,
        elif self.testenv.xrf:
            return env.simxrfgroup,
        elif self.testenv.xrd:
            return env.simxrdgroup,
        else:
            return env.simdiodegroup, env.simtempgroup

    def filenames(self, scan):
        return scanutils.filenames(scan, config_writer=not self.simplewriter)

    def uris(self, scan, subscan):
        return scanutils.scan_uri(scan, subscan=subscan, config_writer=not self.simplewriter)

    @contextmanager
    def _assert_output(self, scan, scanshape=None, masters=None, subscan=1,
                       hastimer=True, checkdata=True):
        if not masters:
            masters = []
        config_writer = not self.simplewriter
        uri = self.uris(scan, subscan)
        filenames = self.filenames(scan)
        for filename in filenames:
            if filename:
                self.assertTrue(os.path.isfile(filename), filename)
        filename = filenames[0]
        with nexus.nxRoot(filename) as root:
            # Find entry
            entry_name = nexus.splitUri(uri)[1]
            self.assertTrue(entry_name in root)
            entry = root[entry_name]
            if checkdata:
                # Check links in master files
                if config_writer:
                    for master in filenames[1:]:
                        if not self.managed:
                            self.assertEqual(master, '')
                            continue
                        with nexus.nxRoot(master) as mroot:
                            for key in mroot:
                                if uri == nexus.getUri(mroot[key]):
                                    break
                            else:
                                self.assertTrue(False, master)
                # Check entry
                self._assert_entry(entry)
                self._assert_positioners(entry, masters=masters,
                                         hastimer=hastimer)
                if config_writer:
                    self._assert_instrument(entry, masters=masters,
                                            hastimer=hastimer)
                    self._assert_measurement(entry, masters=masters,
                                             hastimer=hastimer)
                    self._assert_nxdata(entry, flat=True, masters=masters,
                                        scanshape=scanshape, hastimer=hastimer)
                    self._assert_nxdata(entry, flat=False, masters=masters,
                                        scanshape=scanshape, hastimer=hastimer)
                    self._assert_xrf_app(appli=entry['xrf'],
                                         scanshape=scanshape)
                    # TODO: application definition for XRD
                    self._assert_xrd_app(appli=entry['instrument'],
                                         scanshape=scanshape)
            yield entry

    def _assertmsg(self, group):
        uri = nexus.getUri(group)
        for g in nexus.iterup(group):
            try:
                return '{} ({})'.format(uri, g['title'][()])
            except (KeyError, ValueError, TypeError):
                pass
        return uri

    def _assert_entry(self, entry):
        expected = {'instrument', 'measurement', 'title',
                    'start_time', 'end_time', 'plotselect'}
        if not self.simplewriter:
            expected |= {'all_counters', 'all_spectra',
                         'all_counters_grid', 'all_spectra_grid'}
        if self.testenv.xrf and not self.simplewriter:
            expected -= {'all_spectra', 'all_spectra_grid'}
            expected |= {'all_spectra_samplingcounter',
                         'all_spectra_grid_samplingcounter',
                         'all_spectra_mca', 'all_spectra_grid_mca'}
            expected |= {'xrf', 'xrf_counters', 'xrf_spectra',
                         'xrf_counters_grid', 'xrf_spectra_grid'}
        if self.testenv.xrd and not self.simplewriter:
            expected |= {'all_images', 'xrd_patterns',
                         'all_images_grid', 'xrd_patterns_grid'}
        if self.simplewriter:
            expected |= {'plot0D', 'plot1D_unknown1D'}
            if self.testenv.xrf:
                expected -= {'plot1D_unknown1D'}
                expected |= {'plot1D_unknown1D1', 'plot1D_unknown1D2'}
            if self.testenv.xrd:
                expected |= {'plot2D'}
        self.assertEqual(set(entry.keys()), expected,
                         msg=self._assertmsg(entry))

    def _assert_instrument(self, entry, masters=None, hastimer=True):
        instrument = entry['instrument']
        expected = {'title'}
        expected |= {'positioners',
                     'positioners_start', 'positioners_dial_start',
                     'positioners_end', 'positioners_dial_end'}
        if self.testenv.xrf:
            for i in range(1, 3):
                expected |= {'simxmap{}_det{}'.format(i, j)
                             for j in range(4)}
                expected |= {'simxmap{}_sum'.format(i)}
        if self.testenv.xrd:
            expected |= {'simfrelon'}
        expected |= {'simidet', 'simiodet', 'simfdet'}
        if self.testenv.temperature:
            expected |= {'Tsample', 'Tstage'}
        expected |= {'simabsorbance', 'simidetnorm', 'simfdetnorm'}
        expected |= {'simdiodeMEAN', 'simdiodeSAMPLES', 'simdiodeLAST', 'simdiodeSTATS'}
        expected |= set(masters)
        if hastimer:
            if self.saveoptions['multivalue_positioners']:
                expected |= {'timer'}
            else:
                expected |= {'elapsed_time', 'epoch'}
        self.assertEqual(set(instrument.keys()), expected,
                         msg=self._assertmsg(instrument))

    def _assert_measurement(self, entry, masters=None, hastimer=True):
        measurement = entry['measurement']
        if masters:
            expected = set(masters)
        elif hastimer:
            if self.saveoptions['multivalue_positioners']:
                expected = {'timer'}
            else:
                expected = {'elapsed_time'}
        else:
            self.assertTrue(False, 'No master specified')
        suffixes = ['', '_input_counts', '_input_rate',
                    '_output_counts', '_output_rate',
                    '_dead_time', '_live_time', '_elapsed_time']
        for i in range(1, 3):
            for suffix in suffixes:
                expected |= {'simxmap{}_det{}{}'.format(i, j, suffix)
                             for j in range(4)}
                expected |= {'simxmap{}_det{}_x{}'.format(i, j, k)
                             for j in range(4)
                             for k in range(1, 4)}
                expected |= {'simxmap{}_sum_x{}'.format(i, k)
                             for k in range(1, 4)}
        if self.testenv.xrd:
            expected |= {'simfrelon'}
            expected |= {'simfrelon_roi{}_{}'.format(j, k)
                         for j in range(1, 5)
                         for k in ['min', 'max', 'std', 'sum', 'avg']}
        expected |= {'simidet', 'simiodet', 'simfdet'}
        if self.testenv.temperature:
            expected |= {'Tsample', 'Tstage'}
        expected |= {'simabsorbance', 'simidetnorm', 'simfdetnorm'}
        expected |= {'simdiodeMEAN', 'simdiodeSAMPLES', 'simdiodeLAST', 'simdiodeSTATS'}
        expected |= {'simdiodeSAMPLES_samples', 'simdiodeSTATS_N', 'simdiodeSTATS_min',
                     'simdiodeSTATS_max', 'simdiodeSTATS_p2v', 'simdiodeSTATS_std',
                     'simdiodeSTATS_var'}
        if hastimer:
            if self.saveoptions['multivalue_positioners']:
                expected |= {'pos_'+master for master in ['timer', 'timer_epoch']}
            else:
                expected |= {'pos_'+master for master in ['elapsed_time', 'epoch']}
        self.assertEqual(set(measurement.keys()), expected,
                         msg=self._assertmsg(measurement))

    def _assert_positioners(self, entry, masters=None, hastimer=True):
        instrument = entry['instrument']
        # Validate positioners
        positioners = instrument['positioners']
        expected = {'simx', 'simy', 'simz'}
        expected |= {'simpx', 'simpy', 'simpz'}
        expected |= set(masters)
        if hastimer:
            if self.saveoptions['multivalue_positioners']:
                expected |= {'timer', 'timer_epoch'}
            else:
                expected |= {'elapsed_time', 'epoch'}
        self.assertEqual(set(positioners.keys()), expected,
                         msg=self._assertmsg(positioners))
        # Validate positioner snapshots
        positioners = {'positioners_start', 'positioners_dial_start'}
        if not self.simplewriter:
            positioners |= {'positioners_end', 'positioners_dial_end'}
        expected = {'simx', 'simy', 'simz'}
        expected |= {'simpx', 'simpy', 'simpz'}
        for name in positioners:
            self.assertEqual(set(instrument[name].keys()), expected,
                             msg=self._assertmsg(instrument[name]))
        # Validate motor units
        positioners = {'positioners',
                       'positioners_start', 'positioners_dial_start'}
        if not self.simplewriter:
            positioners |= {'positioners_end', 'positioners_dial_end'}
        for name in positioners:
            for mot in 'simx', 'simy', 'simz':
                self.assertEqual(instrument[name][mot].attrs['units'],
                                 'mm', msg=self._assertmsg(instrument[name]))
            for mot in 'simpx', 'simpy', 'simpz':
                self.assertEqual(instrument[name]['simpz'].attrs['units'],
                                 'um', msg=self._assertmsg(instrument[name]))

    def _assert_nxdata_axes(self, plot, flat=None, detector_ndim=0,
                            masters=None, scanshape=None, hastimer=True):
        if flat is None:
            flat = self.saveoptions['flat']
        if not flat:
            # fast axis last
            masters = masters[::-1]
        # Axes along the scan dimensions
        if scanshape == tuple():
            axes = tuple()
        else:
            if masters:
                axes = tuple(masters)
            elif hastimer:
                if self.saveoptions['multivalue_positioners']:
                    axes = ('timer', )
                else:
                    axes = ('elapsed_time', )
            else:
                self.assertTrue(False, 'No master specified')
        # Axes along the detector dimensions
        if scanshape:
            # This is explicitely done in the writer
            # because a scatter plot of non-scalar data
            # is currently not supported by silx
            noaxes = flat and len(scanshape) > 1 and detector_ndim > 0
        else:
            noaxes = False
        if noaxes:
            axes = tuple()
        else:
            axes = axes + tuple('datadim{}'.format(i)
                                for i in range(detector_ndim))
        self.assertEqual(tuple(plot.attrs.get('axes', tuple())), axes,
                         msg=self._assertmsg(plot))
        return axes

    def _assert_signals(self, plot, signals, flat=None, scanshape=None):
        scanshape = self._saved_scan_shape(scanshape, flat=flat)
        rsignals = nexus.nxDataGetSignals(plot)
        self.assertEqual(set(rsignals), set(signals),
                         msg=self._assertmsg(plot))

    def _assert_nxdata(self, entry, flat=True, masters=None, scanshape=None, hastimer=True):
        if flat:
            suffix = ''
        else:
            suffix = '_grid'
        # Counters plot
        plot = entry['xrf_counters'+suffix]
        axes = self._assert_nxdata_axes(plot,
                                        flat=flat,
                                        detector_ndim=0,
                                        masters=masters,
                                        scanshape=scanshape,
                                        hastimer=hastimer)
        signals = ['simidetnorm', 'simfdetnorm']
        if self.testenv.xrf:
            signals += ['simxmap1_det0_dead_time',
                        'simxmap2_det1_dead_time']
        self._assert_signals(plot, signals,
                             flat=flat,
                             scanshape=scanshape)
        expected = set(axes) | set(signals)
        self.assertEqual(set(plot.keys()), expected,
                         msg=self._assertmsg(plot))
        # Plotselect
        self.assertEqual(entry.get('plotselect', getlink=True).path,
                         entry['xrf_counters'].name,
                         msg=self._assertmsg(entry['plotselect']))
        # XRF spectra plot
        if self.testenv.xrf:
            plot = entry['xrf_spectra'+suffix]
            axes = self._assert_nxdata_axes(plot,
                                            flat=flat,
                                            detector_ndim=1,
                                            masters=masters,
                                            scanshape=scanshape)
            signals = ['simxmap1_det0', 'simxmap2_det1']
            self._assert_signals(plot, signals,
                                 flat=flat,
                                 scanshape=scanshape)
            expected = set(axes) | set(signals)
            self.assertEqual(set(plot.keys()), expected,
                             msg=self._assertmsg(plot))
        # XRD pattern plot
        if self.testenv.xrd:
            plot = entry['xrd_patterns'+suffix]
            axes = self._assert_nxdata_axes(plot,
                                            flat=flat,
                                            detector_ndim=2,
                                            masters=masters,
                                            scanshape=scanshape)
            signals = ['simfrelon']
            self._assert_signals(plot, signals,
                                 flat=flat,
                                 scanshape=scanshape)
            expected = set(axes) | set(signals)
            self.assertEqual(set(plot.keys()), expected,
                             msg=self._assertmsg(plot))

    def _saved_scan_shape(self, scanshape, flat=None):
        if flat is None:
            flat = self.saveoptions['flat']
        if scanshape:
            if flat:
                scanshape = numpy.product(scanshape),
            else:
                scanshape = scanshape[::-1]
        return scanshape

    def _assert_xrf_app(self, appli=None, scanshape=None):
        if not self.testenv.xrf:
            return
        scanshape = self._saved_scan_shape(scanshape)
        # Application definition
        ctrname = 'i0'
        if self.saveoptions['stack_mcas']:
            expected = {'definition', 'start_time', 'end_time',
                        'i0', 'it', 'mca', 'elapsed_time',
                        'live_time'}
            mcaname = 'mca'
            mcastat = 'elapsed_time'
        else:
            expected = {'definition', 'start_time', 'end_time',
                        'i0', 'it', 'mca00', 'mca01',
                        'elapsed_time_mca00', 'elapsed_time_mca01',
                        'live_time_mca00', 'live_time_mca01'}
            mcaname = 'mca00'
            mcastat = 'elapsed_time_mca00'
        self.assertEqual(set(appli.keys()), expected,
                         msg=self._assertmsg(appli))
        # Validate data shape
        mcaconcat = int(self.saveoptions['stack_mcas'])
        variablelength = scanshape is None
        if variablelength:
            scanshape = appli[ctrname].shape
            ctrname = 'it'
        self.assertEqual(appli[ctrname].shape, scanshape,
                         msg=self._assertmsg(appli[ctrname]))
        self.assertEqual(appli[mcastat].shape[mcaconcat:], scanshape,
                         msg=self._assertmsg(appli[mcastat]))
        self.assertEqual(appli[mcaname].shape[mcaconcat:-1], scanshape,
                         msg=self._assertmsg(appli[mcaname]))
        # Validate data
        if not variablelength:
            self._assert_data_filled(appli[ctrname])
            self._assert_data_filled(appli[mcastat])
            self._assert_data_filled(appli[mcaname], axis=-1)

    def _assert_xrd_app(self, appli=None, scanshape=None):
        if not self.testenv.xrd:
            return
        if self.saveoptions['flat'] and scanshape:
            scanshape = numpy.product(scanshape),
        variablelength = scanshape is None
        patternname = 'simfrelon/data'
        # Validate data
        if not variablelength:
            self._assert_data_filled(appli[patternname], axis=(-2, -1))

    def _assert_data_filled(self, dset, axis=tuple()):
        try:
            numpy.array(numpy.nan, dset.dtype)
        except ValueError:
            invalid = dset[()] == 0
        else:
            invalid = numpy.isnan(dset[()])
        # At least one valid value along the detector dimensions
        invalid = invalid.min(axis=axis)
        if invalid.any():
            self.testenv.keepfiles = True
        self.assertFalse(invalid.any(), msg=self._assertmsg(dset))
