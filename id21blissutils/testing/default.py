"""
Create a Bliss YAML configuration
"""
from copy import deepcopy
from . import utils


def _motor_controller_mapping(controller_name, axes):
    return {'controller': {'class': 'mockup',
                           'name': controller_name,
                           'axes': axes}}


def stepper_mapping(motors, **kwargs):
    """
    :param list(str) motors:
    :param **kwargs: see `add_object`
    """
    mapping = {'steps_per_unit': 10000,
               'velocity': 2500.0,
               'acceleration': 1000.0,
               'low_limit': '-.inf',
               'high_limit': '.inf',
               'backlash': 0,
               'unit': 'mm',
               'name': None}
    axes = []
    for name in motors:
        cmapping = deepcopy(mapping)
        cmapping['name'] = name
        utils.add_object(name, **kwargs)
        axes.append(cmapping)
    return _motor_controller_mapping('steppers', axes)


def piezo_mapping(motors, **kwargs):
    """
    :param list(str) motors:
    :param **kwargs: see `add_object`
    """
    mapping = {'steps_per_unit': 10000,
               'velocity': 2500.0,
               'acceleration': 1000.0,
               'low_limit': '-.inf',
               'high_limit': '.inf',
               'backlash': 0,
               'unit': 'um',
               'name': None}
    axes = []
    for name in motors:
        cmapping = deepcopy(mapping)
        cmapping['name'] = name
        utils.add_object(name, **kwargs)
        axes.append(cmapping)
    return _motor_controller_mapping('piezos', axes)


def diode_mapping_list(diodes, mode=None, **kwargs):
    """
    :param **kwargs: see `add_object`
    """
    mapping = {'class': 'simulation_diode',
               'plugin': 'bliss',
               'mode': mode,
               'name': None}
    lst = []
    for name in diodes:
        cmapping = deepcopy(mapping)
        cmapping['name'] = name
        utils.add_object(name, **kwargs)
        lst.append(cmapping)
    return lst


def tempsensor_mapping_list(sensors, **kwargs):
    """
    :param **kwargs: see `add_object`
    """
    mapping = {'class': 'mockup',
               'plugin': 'temperature',
               'outputs': [{'name': None, 'channel': 'A'}]}
    lst = []
    for name in sensors:
        cmapping = deepcopy(mapping)
        cmapping['outputs'][0]['name'] = name
        utils.add_object(name, **kwargs)
        lst.append(cmapping)
    return lst


def mca_mapping_list(mcas, **kwargs):
    """
    :param **kwargs: see `add_object`
    """
    mapping = {'class': 'SimulatedMCA',
               'plugin': 'bliss',
               'module': 'mca',
               'name': None}
    lst = []
    for name in mcas:
        cmapping = deepcopy(mapping)
        cmapping['name'] = name
        utils.add_object(name, **kwargs)
        lst.append(cmapping)
    return lst


def lima_mapping_list(limas, **kwargs):
    """
    :param **kwargs: see `add_object`
    """
    mapping = {'class': 'Lima',
               'plugin': 'bliss',
               'tango_url': 'id00/limaccds/{}1',
               'tango_timeout': 30,
               'name': None}
    lst = []
    for name in limas:
        cmapping = deepcopy(mapping)
        cmapping['name'] = name
        cmapping['tango_url'] = cmapping['tango_url'].format(name)
        utils.add_object(name, **kwargs)
        lst.append(cmapping)
    return lst


def limatango_mapping_list(limas, **kwargs):
    """
    :param **kwargs: see `add_object`
    """
    mapping = {
        'personal_name': None,
        'server': 'LimaCCDs',
        'device': [
            {'class': 'Simulator',
             'tango_name': 'id00/{}/{}1'},
            {'class': 'LimaCCDs',
             'properties': {'LimaCameraType': 'Simulator'},
             'tango_name': 'id00/limaccds/{}1'},
            {'class': 'CtAccumulation',
             'tango_name': 'id00/ctaccumulation/{}1'},
            {'class': 'CtAcquisition',
             'tango_name': 'id00/ctacquisition/{}1'},
            {'class': 'CtBuffer',
             'tango_name': 'id00/ctbuffer/{}1'},
            {'class': 'CtConfig',
             'tango_name': 'id00/ctconfig/{}1'},
            {'class': 'CtEvent',
             'tango_name': 'id00/ctevent/{}1'},
            {'class': 'CtImage',
             'tango_name': 'id00/ctimage/{}1'},
            {'class': 'CtSaving',
             'tango_name': 'id00/ctsaving/{}1'},
            {'class': 'CtShutter',
             'tango_name': 'id00/ctshutter/{}1'},
            {'class': 'CtVideo',
             'tango_name': 'id00/ctvideo/{}1'},
            {'class': 'CtControl',
             'tango_name': 'id00/ctcontrol/{}1'},
            {'class': 'RoiCounterDeviceServer',
             'tango_name': 'id00/roicounter/{}1'},
            {'class': 'BpmDeviceServer',
             'tango_name': 'id00/bpm/{}1'},
            {'class': 'LiveViewer',
             'tango_name': 'id00/liveviewer/{}1'},
            {'class': 'PeakFinderDeviceServer',
             'tango_name': 'id00/peakfinder/{}1'},
            {'class': 'BackgroundSubstractionDeviceServer',
             'tango_name': 'id00/backgroundsubstraction/{}1'},
            {'class': 'FlatfieldDeviceServer',
             'tango_name': 'id00/flatfield/{}1'},
            {'class': 'LimaTacoCCDs',
             'tango_name': 'id00/limatacoccds/{}1'},
            {'class': 'MaskDeviceServer',
             'tango_name': 'id00/mask/{}1'},
            {'class': 'Roi2spectrumDeviceServer',
             'tango_name': 'id00/roi2spectrum/{}1'}
        ]
    }
    lst = []
    for name in limas:
        cmapping = deepcopy(mapping)
        cmapping['personal_name'] = name
        for dic in cmapping['device']:
            args = [name] * dic['tango_name'].count('{}')
            dic['tango_name'] = dic['tango_name'].format(*args)
        utils.add_object(name, **kwargs)
        lst.append(cmapping)
    return lst


def nexus_definitions_mapping():
    applications = {}
    xrf = {'personal_name': 'xrf',
           'class': 'APPxrf',
           'I0': 'simiodet',
           'It': 'simidet',
           'mca': ['simxmap1:det0', 'simxmap2:det1']}
    applications['xrf'] = xrf

    plots = {}
    xrf_counters = ['simidetnorm', 'simfdetnorm',
                    'simxmap1:deadtime_det0',
                    'simxmap2:deadtime_det1']
    xrf_spectra = ['simxmap1:spectrum_det0', 'simxmap2:spectrum_det1']
    xas_counters = ['simabsorbance', 'simfdetnorm',
                    'simxmap1:deadtime_det0',
                    'simxmap2:deadtime_det1']
    xrd_images = ['simfrelon:image']
    fullfield_images = ['simpco:image']
    plots['counters'] = {'personal_name': 'all_counters', 'ndim': 0}
    plots['spectra'] = {'personal_name': 'all_spectra', 'ndim': 1}
    plots['images'] = {'personal_name': 'all_images', 'ndim': 2}
    plots['xas_counters'] = {'personal_name': 'xas_counters', 'items': xas_counters}
    plots['xrf_counters'] = {'personal_name': 'xrf_counters', 'items': xrf_counters}
    plots['xrf_spectra'] = {'personal_name': 'xrf_spectra', 'items': xrf_spectra}
    plots['xrd'] = {'personal_name': 'xrd_patterns', 'items': xrd_images}
    plots['fullfield'] = {'personal_name': 'fullfield_images', 'items': fullfield_images}
    for k, v in list(plots.items()):
        v = dict(v)
        v['personal_name'] = v['personal_name'] + '_grid'
        v['grid'] = True
        plots[k + '_grid'] = v

    techniques = {}
    techniques['xrf'] = {'applications': ['xrf'],
                         'plots': ['xrf_counters', 'xrf_spectra', 'counters', 'spectra']}
    techniques['xas'] = {'applications': ['xrf'],
                         'plots': ['xas_counters', 'xrf_spectra', 'counters', 'spectra']}
    techniques['xrfxrd'] = {'applications': ['xrf'],
                            'plots': ['xrf_counters', 'xrf_spectra', 'xrd', 'counters', 'spectra', 'images']}
    techniques['fullfield'] = {'applications': [],
                               'plots': ['fullfield', 'images']}
    for v in techniques.values():
        v['plots'] += [k+'_grid' for k in v['plots']]

    mdata = {'default': 'tango://lid21sxm:20000/blisstest/metadata/ing-blisstest',
             'xrf': 'tango://lid21sxm:20000/blisstest/metadata/ing-blisstest'}
    mapping = {'name': 'nexus_definitions',
               'mdata': mdata,
               'technique': {'default': 'xrf',
                             'techniques': techniques,
                             'applications': applications,
                             'plots': plots}
              }
    return mapping


def acq_chain_settings(limas=None):
    settings = []
    for lima in limas:
        settings.append({'device': lima,
                         'acquisition_settings': {
                            'saving_format': 'HDF5',
                            'saving_suffix': '.h5'
                         }})
    return {'name': 'default_acq_chain',
            'plugin': 'default',
            'chain_config': settings}


def create(session_name, xrf=True, xrd=True,
           fullfield=True, temperature=True):
    """
    Creates a test session as YAML files (not in-memory yet)

    :param str session_name:
    """
    nodes_to_save = []
    nodes_to_keep = []  # prevents deleting before saving

    session_objects = []
    session_aliases = []
    xrfgroup = []
    xrdgroup = []
    ffgroup = []
    tempgroup = []
    diodegroup = []
    user_definitions = ['from bliss.common.standard import *',
                        'from id21blissutils import *',
                        'id21blissutils_setup()']
    #user_definitions = ['from bliss.common.standard import *']

    session_kwargs = {'session_objects': session_objects,
                      'session_aliases': session_aliases}

    mapping = {'synchrotron': 'ESRF',
               'beamline': 'ID00'}
    root = utils.create_dirnode(None,
                                mapping=mapping)
    nodes_to_save.append(root)

    mapping = {'plugin': 'session'}
    sessions = utils.create_dirnode('sessions',
                                    parent=root,
                                    mapping=mapping)
    nodes_to_save.append(sessions)

    mapping = {'plugin': 'emotion'}
    motors = utils.create_dirnode('motors',
                                  parent=root,
                                  mapping=mapping)
    nodes_to_save.append(motors)

    mapping = None
    tangoservers = utils.create_dirnode('tango',  # the name 'tango' is mandatory
                                        parent=root,
                                        mapping=mapping)
    nodes_to_save.append(tangoservers)

    mapping = None
    detectors = utils.create_dirnode('detectors',
                                     parent=root,
                                     mapping=mapping)
    nodes_to_save.append(detectors)

    mapping = None
    scans = utils.create_dirnode('scans',
                                 parent=root,
                                 mapping=mapping)
    nodes_to_save.append(scans)

    mapping = utils.session_mapping(session_name, **session_kwargs)
    node = utils.create_filenode(session_name,
                                 parent=sessions,
                                 mapping=mapping)
    nodes_to_save.append(node)

    # Motors

    mapping = piezo_mapping(['simpx', 'simpy', 'simpz'], **session_kwargs)
    node = utils.create_filenode(mapping['controller']['name'],
                                 parent=motors,
                                 mapping=mapping)
    nodes_to_save.append(node)

    mapping = stepper_mapping(['simx', 'simy', 'simz'], **session_kwargs)
    node = utils.create_filenode(mapping['controller']['name'],
                                 parent=motors,
                                 mapping=mapping)
    nodes_to_save.append(node)

    # Detectors/sensors

    lst = []
    #modes = 'MEAN', 'STATS', 'SAMPLES', 'SINGLE', 'LAST', 'INTEGRATE', 'INTEGRATE_STATS'
    modes = 'MEAN', 'STATS', 'SAMPLES', 'LAST'
    for mode in modes:
        lst += diode_mapping_list(['simdiode' + mode], mode=mode, xrfgroup=xrfgroup,
                                  xrdgroup=xrdgroup, diodegroup=diodegroup,
                                  **session_kwargs)
    if xrf or xrd:
        lst += diode_mapping_list(['simiodet'], xrfgroup=xrfgroup,
                                  xrdgroup=xrdgroup, diodegroup=diodegroup,
                                  **session_kwargs)
    if xrf:
        lst += diode_mapping_list(['simidet', 'simfdet'], xrfgroup=xrfgroup,
                                  diodegroup=diodegroup, **session_kwargs)
        user_definitions += ['',
                             'from id21blissutils.utils.calcounter import absorbance as _factory',
                             "simabsorbance = _factory('simabsorbance', simidet, simiodet)",
                             'from id21blissutils.utils.calcounter import ratio as _factory',
                             "simidetnorm = _factory('simidetnorm', simfdet, simiodet)",
                             "simfdetnorm = _factory('simfdetnorm', simfdet, simiodet)"]
        utils.add_object('simabsorbance', xrfgroup=xrfgroup,
                         diodegroup=diodegroup)
        utils.add_object('simfdetnorm', xrfgroup=xrfgroup,
                         diodegroup=diodegroup)
        utils.add_object('simidetnorm', xrfgroup=xrfgroup,
                         diodegroup=diodegroup)
    for mapping in lst:
        node = utils.create_filenode('diodes',
                                     parent=detectors,
                                     mapping=mapping)
        nodes_to_keep.append(node)
    nodes_to_save.append(node)

    lst = []
    if temperature:
        lst += tempsensor_mapping_list(['Tsample', 'Tstage'],
                                       tempgroup=tempgroup, **session_kwargs)
    for mapping in lst:
        node = utils.create_filenode('sensors',
                                     parent=detectors,
                                     mapping=mapping)
        nodes_to_keep.append(node)
    nodes_to_save.append(node)

    lst = []
    if xrf:
        lst += mca_mapping_list(['simxmap1', 'simxmap2'],
                                xrfgroup=xrfgroup, **session_kwargs)
    for mapping in lst:
        node = utils.create_filenode('mca',
                                     parent=detectors,
                                     mapping=mapping)
        nodes_to_keep.append(node)
    nodes_to_save.append(node)

    lst = []
    if xrd:
        lst += limatango_mapping_list(['simfrelon'])
    if fullfield:
        lst += limatango_mapping_list(['simpco'])
    for mapping in lst:
        node = utils.create_filenode('lima',
                                     parent=tangoservers,
                                     mapping=mapping)
        nodes_to_keep.append(node)
    nodes_to_save.append(node)

    lst = []
    limas = []
    if xrd:
        lst += lima_mapping_list(['simfrelon'], xrdgroup=xrdgroup,
                                 **session_kwargs)
        limas.append('simfrelon')
    if fullfield:
        lst += lima_mapping_list(['simpco'], ffgroup=ffgroup,
                                 **session_kwargs)
        limas.append('simfrelon')
    for mapping in lst:
        node = utils.create_filenode('lima',
                                     parent=detectors,
                                     mapping=mapping)
        nodes_to_keep.append(node)
    nodes_to_save.append(node)

    # Measurement groups

    mapping = {'class': 'MeasurementGroup',
               'name': 'simdiodegroup',
               'counters': diodegroup}
    utils.add_object(mapping['name'], **session_kwargs)
    node = utils.create_filenode(session_name,
                                 parent=sessions,
                                 mapping=mapping)
    nodes_to_keep.append(node)

    if temperature:
        mapping = {'class': 'MeasurementGroup',
                   'name': 'simtempgroup',
                   'counters': tempgroup}
        utils.add_object(mapping['name'], **session_kwargs)
        node = utils.create_filenode(session_name,
                                     parent=sessions,
                                     mapping=mapping)
        nodes_to_keep.append(node)

    if xrf:
        mapping = {'class': 'MeasurementGroup',
                   'name': 'simxrfgroup',
                   'counters': list(set(xrfgroup+tempgroup))}
        utils.add_object(mapping['name'], **session_kwargs)
        node = utils.create_filenode(session_name,
                                     parent=sessions,
                                     mapping=mapping)
        nodes_to_keep.append(node)

    if xrd:
        mapping = {'class': 'MeasurementGroup',
                   'name': 'simxrdgroup',
                   'counters': list(set(xrdgroup + tempgroup))}
        utils.add_object(mapping['name'], **session_kwargs)
        node = utils.create_filenode(session_name,
                                     parent=sessions,
                                     mapping=mapping)
        nodes_to_keep.append(node)

    if fullfield:
        mapping = {'class': 'MeasurementGroup',
                   'name': 'simffgroup',
                   'counters': ffgroup}
        utils.add_object(mapping['name'], **session_kwargs)
        node = utils.create_filenode(session_name,
                                     parent=sessions,
                                     mapping=mapping)
        nodes_to_keep.append(node)

    if xrf and xrd:
        mapping = {'class': 'MeasurementGroup',
                   'name': 'simxrfxrdgroup',
                   'counters': list(set(xrfgroup + xrdgroup + tempgroup))}
        utils.add_object(mapping['name'], **session_kwargs)
        node = utils.create_filenode(session_name,
                                     parent=sessions,
                                     mapping=mapping)
        nodes_to_keep.append(node)

    # Scan info
    mapping = nexus_definitions_mapping()
    node = utils.create_filenode(mapping['name'],
                                 parent=scans,
                                 mapping=mapping)
    nodes_to_save.append(node)

    mapping = acq_chain_settings(limas=limas)
    node = utils.create_filenode(mapping['name'],
                                 parent=scans,
                                 mapping=mapping)
    nodes_to_save.append(node)
    if mapping['chain_config']:
        user_definitions += ['',
                             'from bliss.config import static',
                             "DEFAULT_CHAIN.set_settings(static.get_config().get('{}').to_dict()['chain_config'])"
                             .format(mapping['name'])]

    # Save YAML files and python scripts
    for node in nodes_to_save:
        node.save()
    utils.save_session_setup_script(session_name, sessions)
    utils.save_session_user_script(session_name, sessions, user_definitions)
