"""
Create a Bliss Test Environement
"""
import os
import logging
from testfixtures import TempDirectory
from contextlib import contextmanager
import atexit
import warnings
import bliss
from bliss.config import static
from bliss.controllers.lima.roi import Roi as LimaRoi
from bliss.controllers.mca.base import BaseMCA
import tango
from ..server import beacon
from ..server import lima
from .utils import create_nonexisting
from . import default


logger = logging.getLogger(__name__)


def get_config():
    try:
        return static.get_config()
    except OSError as e:
        # Non-existing configuration directory
        logger.debug(e)
        return None
    except RuntimeError as e:
        # No Beacon server found
        # or wrong BEACON_HOST=host:port
        logger.debug(e)
        return None


def beacon_connection_clean(connection):
    """
    Clean Beacon connection and its Redis connections

    :param bliss.config.conductor.connection.Connection connection:
    """
    connection.clean_all_redis_connection()
    connection.close()
    connection._socket = None
    connection._host = None
    connection._port = None
    connection._clean()


def global_bliss_cleanup():
    """
    Clean all globals that hold beacon/redis connections
    """
    config = get_config()
    if config:
        config.close()
        beacon_connection_clean(config._connection)
    bliss.global_map.clear()
    # Drastic cleanup
    #del static.client._default_connection
    #static.client._default_connection = None
    #del static.CONFIG
    #static.CONFIG = None
    # Run all functions registered with `weakref.finalize`.
    # For example: bliss.data.node.DataNode registers its `set_ttl` methods
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")  # e.g. tempdirectory cleanup warning
        atexit._run_exitfuncs()


class SessionEnv(object):
    """
    Session environment wrapper that exposes session objects as attributes

    For example current motor position:
        instance.wm(instance.simx)
    """

    def __init__(self, session_name):
        self.session_name = session_name

    @property
    def config(self):
        """
        In-memory configuration
        """
        return get_config()

    @property
    def session(self):
        """
        bliss.common.session.Session instance
        """
        return self.config.get(self.session_name)

    @property
    def variables(self):
        """
        Session namespace
        """
        return self.session.env_dict

    @property
    def objects(self):
        """
        Session objects
        """
        vars = self.variables
        return {k: vars[k] for k in self.names(objects=True)}

    def objects_of_type(self, *classes):
        vars = self.variables
        ret = {}
        for k in self.names(objects=True):
            v = vars[k]
            if isinstance(v, classes):
                ret[k] = v
        return ret

    def names(self, objects=False):
        if objects:
            # Subset of variables
            return self.session.object_names
        else:
            return list(self.variables.keys())

    def __getattr__(self, attr):
        try:
            return self.variables[attr]
        except KeyError:
            raise AttributeError

    def load(self):
        """
        Load YAML database in memory and add references to Session instance
        """
        # YAML --instantiate--> in-memory
        self.config.reload()
        # in-memory --reference--> Session instance
        self.session.setup(verbose=logger.getEffectiveLevel() <= logging.INFO)
        # This means that id(self.variables[var]) == id(self.config.get(var))
        # self.session.resetup()  #???

    def close(self):
        """
        Close session and Beacon connection
        """
        try:
            self.session.close()
        except RuntimeError:
            pass


class BlissTestEnv(object):
    """
    Launch a Beacon server (+Redis+TangoDb)
    with a configuration database (creates YAML files when needed).
    """

    def __init__(self, blissenv=False, db_path=None,
                 session_name=None, lima_servers=None,
                 webapp=False, keepfiles=False,
                 create=None, **session_kwargs):
        # Make sure we don't find an existing Beacon host
        # by the discovery mechanism.
        os.environ['BEACON_HOST'] = "xxxxxx:xxxxxx"

        if not lima_servers:
            lima_servers = []
        self._env = {'blissenv': blissenv,
                     'db_path': db_path,
                     'session_name': session_name,
                     'lima_servers': lima_servers}
        self._webapp = webapp
        self.keepfiles = keepfiles
        self._dir = None
        self._beacon = None
        self._lima_servers = []
        self.session_env = None
        self._session_kwargs = session_kwargs
        if create is None:
            create = default.create
        self._create = create

    @property
    def xrf(self):
        return self._session_kwargs.get('xrf', True)

    @property
    def xrd(self):
        return self._session_kwargs.get('xrd', True)

    @property
    def fullfield(self):
        return self._session_kwargs.get('fullfield', True)

    @property
    def temperature(self):
        return self._session_kwargs.get('temperature', True)

    def _init_setup(self):
        self.cleanup()
        self._init_setup_params()
        self._beacon = beacon.Beacon(db_path=self._env['db_path'],
                                     webapp_port=self._webapp)
        tango_servers = self._env['lima_servers']
        self._beacon.environment(load=True)
        self._lima_servers = [lima.Lima(name, tango_uri=uri)
                              for name, uri in tango_servers]

    def _init_setup_params(self):
        self._dir = None
        session_name = self._env['session_name']
        if not session_name:
            self._env['session_name'] = session_name = 'test_session'
        if self._env['blissenv']:
            # This is the bliss test suite
            # (only works with a bliss 'pip install -e')
            db_path = os.path.join(bliss.__path__[0],
                                   '..', 'tests', 'test_configuration')
            if os.path.exists(db_path):
                self._env['db_path'] = db_path
                self._env['select_session_name'] = session_name
                self._env['lima_servers'] = [('simulator',
                                              'id00/limaccds/simulator1')]
                return
        if self._env['db_path']:
            # This is a custom environment
            if os.path.exists(self._env['db_path']):
                self._env['select_session_name'] = session_name
                return
        # This is the id21blissutils test suite
        # (see id21blissutils.utils.session)
        self._dir = TempDirectory()
        self._env['db_path'] = self._dir.path
        lst = []
        if self.xrd:
            lst.append(('simfrelon', 'id00/limaccds/simfrelon1'))
        if self.fullfield:
            lst.append(('simpco', 'id00/limaccds/simpco1'))
        self._env['lima_servers'] = lst
        self._env['select_session_name'] = None

    def cleanup(self, keepfiles=False):
        if self.session_env is not None:
            self.session_env.close()
            self.session_env = None
        global_bliss_cleanup()
        for server in self._lima_servers:
            server.stop()
        self._lima_servers = []
        if self._beacon is not None:
            self._beacon.stop()
            self._beacon = None
        if self._dir is not None and not (self.keepfiles or keepfiles):
            self._dir.cleanup()
        self._dir = None
        tango.ApiUtil.cleanup()

    def setup(self):
        self._init_setup()
        with self.cleanupContext():
            self._beacon.start()
            bnew = self._create_env()
            servers = self._lima_servers
            if bnew and servers:
                # Start/stop Beacon to fill the Tango Database
                self._beacon.stop()
                # because beacon socket will changed:
                global_bliss_cleanup()
                self._beacon.start()
            for server in servers:
                # TODO: Unknown exception while trying to fill database cache...
                #https://github.com/tango-controls/pytango/issues/309
                server.start()
            self.session_env.load()
            self._create_lima_rois()
            self._create_mca_rois()

    def _create_lima_rois(self):
        rois = {'roi1': LimaRoi(0, 0, 100, 200),
                'roi2': LimaRoi(10, 20, 200, 500),
                'roi3': LimaRoi(20, 60, 500, 500),
                'roi4': LimaRoi(60, 20, 50, 10)}
        session_env = self.session_env
        for name, tangouri in self._env['lima_servers']:
            controller = session_env.objects[name]  # TODO: uses TANGO_HOST from first test
            controller.proxy.ping()
            controller.roi_counters.update(rois)

    def _create_mca_rois(self):
        rois = {'x1': (80, 110),
                'x2': (80, 110),
                'x3': (80, 110)}
        for mca in self.session_env.objects_of_type(BaseMCA).values():
            for name, roi in rois.items():
                mca.rois.set(name, *roi)

    def setUp(self):
        self._log_start('Setup Bliss Test Environment')
        self.setup()
        self._log_end('Setup Bliss Test Environment')

    def tearDown(self):
        self._log_start('Tear down Bliss Test Environment')
        self.cleanup()
        self._log_end('Tear down Bliss Test Environment')

    @contextmanager
    def cleanupContext(self):
        try:
            yield
        except Exception:
            self._log_start('Premature tear down Bliss Test Environment')
            self.cleanup(keepfiles=True)
            self._log_end('Premature tear down Bliss Test Environment')
            raise

    def __enter__(self):
        self.setUp()
        return self

    def __exit__(self, etype, value, traceback):
        self.tearDown()
        # return 1: means I handled the exception
        return etype == KeyboardInterrupt

    def _log_start(self, msg):
        fmt = '\n\n========={}: start========='
        logger.info(fmt.format(msg))

    def _log_end(self, msg):
        fmt = '\n========={}: done=========\n'
        logger.info(fmt.format(msg))

    def _create_env(self):
        """
        Create a Bliss session (YAML files) unless it already exists
        """
        bnew = False
        logger.info('Create session environment ...')
        if not self._beacon.isrunning:
            raise RuntimeError('Beacon server is not running')
        static.get_config().reload()  # clear configuration
        name = self._env['select_session_name']
        if not name:
            name = self._env['session_name']
            name = create_nonexisting(self._create, name,
                                      **self._session_kwargs)
            bnew = True
        self.session_env = SessionEnv(name)
        logger.info('Session environment {} created'.format(repr(name)))
        return bnew

    @property
    def blissclient(self):
        lst = self._beacon.blissclient
        lst += ['bliss', '-s', self.session_env.session_name, '--no-tmux']
        level = logger.getEffectiveLevel()
        loglevel = beacon.level_to_cli.get(level, 'WARN')
        lst += ['--log', loglevel]
        return ' '.join(lst)

    @property
    def tangoclient(self):
        lst = self._beacon.tangoclient
        lst.append('jive')
        return ' '.join(lst)

    @property
    def writerserver(self):
        lst = self._beacon.blissclient
        lst += ['python', '-m', 'id21blissutils.server.nxscanwriter',
                self.session_env.session_name]
        return ' '.join(lst)

    @property
    def webclient(self):
        lst = self._beacon.webclient
        if lst:
            lst = ['firefox'] + lst
        return ' '.join(lst)

    @property
    def redisclient(self):
        lst = ['redis-cli', '-s']
        lst += self._beacon.redisclient
        lst += ['-n', '0']
        return ' '.join(lst)
