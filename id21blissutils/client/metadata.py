"""
A client for MetadataManager:
 https://gitlab.esrf.fr/icat/tango-metadata
"""

# Original:
# https://gitlab.esrf.fr/denolf/Beamline-data-policy-doc/blob/master/python/MedataManagerClient.py

import os
import re
import functools
import logging
from collections import OrderedDict
from bliss.common.tango import DeviceProxy, DevFailed
from ..io.utils import mkdir


logger = logging.getLogger(__name__)


STATES = {'OFF': 'No experiment ongoing',
          'STANDBY': 'Experiment started, sample or dataset not specified',
          'ON': 'No dataset running',
          'RUNNING': 'Dataset is running',
          'FAULT': 'Device is not functioning correctly'}


class StateException(Exception):
    """
    Base class for manager/device state exceptions.
    """
    pass


def requiresState(required_before=None, required_after=None,
                  forbidden_before=None, forbidden_after=None):
    """
    Decorate client method that requires particular states

    :param list or str required_before: allowed manager state before
                                        the function call
    :param list or str required_after: allowed manager state after
                                       the function call
    :param list or str forbidden_before: forbidden manager state before
                                         the function call
    :param list or str forbidden_after: forbidden manager state after
                                        the function call
    """
    if required_before:
        if not isinstance(required_before, (list, tuple)):
            required_before = [required_before]
    if required_after:
        if not isinstance(required_after, (list, tuple)):
            required_after = [required_after]
    if forbidden_before:
        if not isinstance(forbidden_before, (list, tuple)):
            forbidden_before = [forbidden_before]
    if forbidden_after:
        if not isinstance(forbidden_after, (list, tuple)):
            forbidden_after = [forbidden_after]

    def wrap(method):
        @functools.wraps(method)
        def inner(self, *args, **kwargs):
            if required_before:
                self._raiseIfNotManagerState(*required_before, when=' before')
            if forbidden_before:
                self._raiseIfManagerState(*forbidden_before, when=' before')
            ret = method(self, *args, **kwargs)
            if required_after:
                self._raiseIfNotManagerState(*required_after, when=' after')
            if forbidden_after:
                self._raiseIfManagerState(*forbidden_after, when=' after')
            return ret
        return inner
    return wrap


def requiresOnline(method):
    """
    Decorate client method that requires servers to be online
    """
    @functools.wraps(method)
    def inner(self, *args, **kwargs):
        self._raiseIfNotOnline()
        return method(self, *args, **kwargs)
    return inner


def MDeviceProxy(uri):
    if not uri:
        return DummyProxy('Null Proxy')
    try:
        return DeviceProxy(uri)
    except DevFailed as e:
        e = '\n '.join(str(e).split('\n'))
        print('Cannot get proxy to {} because:\n {}'
              .format(repr(uri), e))
        return DummyProxy(uri)


class DummyProxy(object):

    def __init__(self, uri):
        self.uri = uri

    def __repr__(self):
        return repr(self.uri)

    def __getattr__(self, attr):
        raise DevFailed('Proxy could not be created (cannot connect to Tango database)')


class MetadataClient(object):
    """
    A client for the MetadataManager tango Devices.
    An instance of this class does not contain any state.
    """

    def __init__(self, uri):
        """
        :param str uri: MetadataManager tango uri
        """
        self.metadataManager = MDeviceProxy(uri)
        self._metaExperiment = None

    @property
    def metaExperiment(self):
        # Currently cached
        if self._metaExperiment is None:
            mgr = self.metadataManager
            if isinstance(mgr, DummyProxy):
                self._metaExperiment = MDeviceProxy('')
            else:
                props = mgr.get_property('metaExperimentDevice',
                                         value=None)
                lst = props['metaExperimentDevice']
                if lst:
                    uri = 'tango://{}:{}/{}'.format(mgr.get_db_host(),
                                                    mgr.get_db_port(),
                                                    lst[0])
                    self._metaExperiment = MDeviceProxy(uri)
                else:
                    raise RuntimeError('{} does not have a property \'metaExperimentDevice\''
                                       .format(mgr))
        return self._metaExperiment

    def __repr__(self):
        return repr(self.metadataManager)

    def _raiseIfNotOnline(self):
        for server, state in self.serverStates.items():
            if state == 'OFFLINE':
                raise StateException('{} is OFFLINE'.format(server))

    def _raiseIfManagerState(self, *mgrstates, when=''):
        mgrstate = self.state
        if mgrstate in mgrstates:
            raise StateException('{} status {} ({}) is not expected{}'
                                 .format(self.metadataManager, mgrstate,
                                         STATES[mgrstate], when))

    def _raiseIfNotManagerState(self, *mgrstates, when=''):
        mgrstate = self.state
        if mgrstate not in mgrstates:
            raise StateException('{} status {}\n({})\nwhile {} is required{}'
                                 .format(self.metadataManager, mgrstate,
                                         STATES[mgrstate], mgrstates, when))

    @property
    def saveinfo(self):
        info = OrderedDict()
        for server, state in self.serverStates.items():
            info[str(server)] = state
        attrs = ['status', 'template', 'path', 'root', 'experiment',
                 'beamline', 'sample', 'dataset', 'technique']
        for attr in attrs:
            try:
                info[attr] = getattr(self, attr, None)
            except StateException as e:
                info[attr] = None
                msg = ['Cannot retrieve {} because:'.format(repr(attr))]
                msg += str(e).split('\n')
                print('\n '.join(msg))
        return info

    def printStatus(self):
        fmt = '{:10}: {}'
        for k, v in self.saveinfo.items():
            print(fmt.format(k, v))

    @property
    def serverStates(self):
        states = {}
        for proxy in self.metaExperiment, self.metadataManager:
            try:
                proxy.ping()
                proxy.state()
            except DevFailed:
                states[str(proxy)] = 'OFFLINE'
            else:
                states[str(proxy)] = 'ONLINE'
        return states

    @property
    def online(self):
        return set(self.serverStates.values()) == {'ONLINE'}

    def __bool__(self):
        return self.online

    @property
    @requiresOnline
    def state(self):
        return self.metadataManager.scanState

    @property
    def status(self):
        state = self.state
        return '{} ({})'.format(state, STATES[state])

    @property
    @requiresOnline
    def path(self):
        """
        Absolute dataset directory
        """
        return self.metadataManager.GetDefaultFolder()

    @property
    @requiresOnline
    def template(self):
        if self.metadataManager.GetDatasetParentLocation():
            return None
        else:
            return self.metadataManager.GetDataFolderPattern()

    @template.setter
    def template(self, value):
        self.metadataManager.ClearDatasetParentLocation()
        if self.template != value:
            print("Set 'dataFolderPattern' = '{}' in {} and restart it"
                  .format(value, self.metadataManager))

    @property
    @requiresOnline
    def template_variables(self):
        ret = {}
        for attr in re.findall(r'\{(.*?)\}', self.template):
            try:
                value = getattr(self.metadataManager, attr)
            except AttributeError:
                value = getattr(self.metaExperiment, attr)
            ret[attr] = value
        return ret

    @property
    @requiresOnline
    def ingesterDebugCommand(self):
        s = str(self.metadataManager.Status())
        server = re.search(r'server=([^\s]+)', s).group(1)
        return 'ssh blissadm@{} "tail -f logs/icat_ingester.log"'.format(server)

    @property
    @requiresState(forbidden_before='FAULT')
    def root(self):
        """
        e.g. /data/visitor
        """
        return self.metaExperiment.dataRoot

    @root.setter
    @requiresState(required_before='STANDBY')
    def root(self, value):
        self.metaExperiment.dataRoot = value

    @property
    @requiresState(forbidden_before='FAULT')
    def proposal(self):
        return self.metaExperiment.proposal

    @proposal.setter
    @requiresState(forbidden_before=['FAULT', 'RUNNING'],
                   required_after='STANDBY')
    def proposal(self, value):
        self.metaExperiment.proposal = value
        # Resets dataRoot to '/data/visitor'

    @requiresState(required_after='OFF')
    def reset(self, force=False):
        try:
            self.metaExperiment.proposal = ''
        except DevFailed:
            if force:
                self.metadataManager.InterruptScan()
                self.metaExperiment.proposal = ''

    @property
    @requiresOnline
    def beamline(self):
        return self.metadataManager.beamlineID

    @property
    @requiresState(forbidden_before=['FAULT', 'OFF'])
    def sample(self):
        return self.metaExperiment.sample

    @sample.setter
    @requiresState(required_before=['STANDBY', 'ON'],
                   required_after=['STANDBY', 'ON'])
    def sample(self, value):
        self.metaExperiment.sample = value

    @property
    @requiresState(forbidden_before=['FAULT', 'OFF'])
    def dataset(self):
        value = self.metadataManager.scanName
        prefix = self.sample + '_'
        if value.startswith(prefix):
            value = value[len(prefix):]
        return value

    @dataset.setter
    @requiresState(required_before=['STANDBY', 'ON'],
                   required_after='ON')
    def dataset(self, value):
        self.metadataManager.scanName = self.sample + '_' + value

    @property
    @requiresOnline
    def technique(self):
        return self.metadataManager.definition

    @technique.setter
    @requiresState(forbidden_before=['FAULT'])
    def technique(self, value):
        if self.state != 'RUNNING':
            logger.info('Setting technique to {} will be reset by `startDataset`'.format(repr(value)))
        self.metadataManager.definition = value

    @requiresOnline
    def get_icat_attribute(self, name):
        return getattr(self.metadataManager, name)

    @requiresState(forbidden_before=['FAULT'])
    def set_icat_attribute(self, name, value):
        if self.state != 'RUNNING':
            logger.info('Setting {} to {} will be reset by `startDataset`'.format(repr(name), repr(value)))
        setattr(self.metadataManager, name, value)

    @requiresState(required_before='RUNNING')
    def appendFile(self, filePath):
        self.metadataManager.lastDataFile = filePath

    @requiresState(required_before='ON',
                   required_after='RUNNING')
    def startDataset(self, create_directory=True):
        """
        Start dataset: initialize ingestion of files on the dataset path
        """
        if create_directory:
            mkdir(self.path)
        self.metadataManager.StartScan()

    @requiresState(required_before='RUNNING',
                   required_after='STANDBY')
    def endDataset(self):
        """
        End dataset:
            * finds all files in dataset folder
            * save with metadata in local HDF5
            * send metadata to ICAT
        """
        self.metadataManager.endScan()

    @requiresState(required_before='RUNNING',
                   required_after='STANDBY')
    def interruptDataset(self):
        """
        Interrupt dataset: currently the same effect as endDataset
        """
        self.metadataManager.InterruptScan()

    @requiresState(required_before='RUNNING',
                   required_after='ON')
    def pauseDataset(self):
        """
        Pause dataset:
            * does not send anything to HDF5 or ICAT
            * puts us back in the state right required_before `startDataset` was called
            * call `startDataset` to resume
        """
        self.metadataManager.AbortScan()

    @requiresOnline
    def elogmessage(self, *msg, client=None, type=None):
        msg = '\n'.join(msg)
        if client is not None:
            msg = '[{}]: {}'.format(client, msg)
        if type == 'error':
            self.metadataManager.notifyError(msg)
        elif type == 'command':
            self.metadataManager.notifyCommand(msg)
        elif type == 'info':
            self.metadataManager.notifyInfo(msg)
        elif type == 'user':
            self.metadataManager.userComment(msg)
        else:
            self.metadataManager.notifyDebug(msg)


def id21test():
    client = MetadataClient('tango://lid21sxm:20000/blisstest/metadata/ing-blisstest')

    # Put in the OFF state
    print('\nRESET\n')
    client.reset(force=True)
    client.printStatus()

    print('\nPROPOSAL\n')
    #client.proposal = 'ip0001'
    client.proposal = 'id211907'
    client.printStatus()

    print('\nROOT\n')
    #client.root = '/data/id21/inhouse/19feb'
    client.root = '/data/id21/inhouse/default'
    client.printStatus()

    print('\nSAMPLE\n')
    client.sample = 'align'
    client.printStatus()

    print('\nDATASET\n')
    client.dataset = 1
    client.printStatus()

    print('\nSTART\n')
    client.startDataset()
    client.printStatus()
    print('')

    client.technique = 'abc'

    client.elogmessage('New test run ' + client.dataset)

    # Create some data
    path = client.path
    mkdir(path)
    filename = os.path.join(path, 'test.txt')
    with open(filename, 'w') as fp:
        fp.write('test')

    #client.appendFile(filename)

    # Adding metadata
    #client.metadataManager.scanType = "MX"
    #client.metadataManager.InstrumentSource_energy = "12.834 keV"
    #client.metadataManager.MX_transmission = "100%"
    #client.metadataManager.MX_flux = "1.5e+11 ph/sec"
    #client.metadataManager.MX_fluxEnd = "1.49e+11 ph/sec"
    #client.metadataManager.MX__resolution = "3.75 Å"

    print('\nEND\n')
    client.endDataset()
    client.printStatus()

    print('')
    print(client.ingesterDebugCommand)


if __name__ == '__main__':
    id21test()
