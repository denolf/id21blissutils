__all__ = [
    '__project__',
    '__description__',
    '__url__',
    '__version__',
]

__project__ = 'id21blissutils'
__description__ = 'ID21 Bliss Utilities'
__url__ = 'https://gitlab.esrf.fr/denolf/id21blissutils'
__version__ = '0.0.2a0'
