"""
Bliss scan utilities
"""

from . import config
from nexus_writer_service.utils import scan_utils


def filenames(info, config_writer=True):
    """
    :param bliss.scanning.scan.Scan or dict scan_info:
    :param bool config_writer: expect configurable writer
    :returns list(str):
    """
    if not isinstance(info, dict):
        info = info.scan_info
    if config_writer:
        try:
            lst = info['external']['filenames']
        except KeyError:
            lst = scan_utils.filenames()
    else:
        try:
            filename = info['filename']
        except KeyError:
            filename = scan_utils.internal_filename()
        filename = scan_utils.scan_filename_int2ext(filename)
        lst = [filename]
    lst += [''] * max(3-len(lst), 0)
    return lst


def scan_uri(info, subscan=1, config_writer=True):
    """
    :param bliss.scanning.scan.Scan or dict scan_info:
    :param bool config_writer: expect configurable writer
    :returns str:
    """
    if not isinstance(info, dict):
        info = info.scan_info
    filename = filenames(info, config_writer=config_writer)[0]
    if filename:
        return filename + '::/' + scan_utils.scan_name(info, subscan=subscan)
    else:
        return ''


def dataset_uri(info, config_writer=True):
    """
    :param bliss.scanning.scan.Scan or dict scan_info:
    :returns str:
    """
    return filenames(info, config_writer=config_writer)[0]


def last_uri(config_writer=True):
    """
    Last scan with saving enabled or current dataset

    :param bool config_writer: expect configurable writer
    :returns str: scan or dataset uri
    """
    for scan in reversed(config.last_scans()):
        if scan.scan_info['save']:
            return scan_uri(scan, config_writer=config_writer)
    return filenames({}, config_writer=config_writer)[0]


def sample_uri(info, config_writer=True):
    """
    :param bliss.scanning.scan.Scan or dict scan_info:
    :param bool config_writer: expect configurable writer
    :returns str:
    """
    return filenames(info, config_writer=config_writer)[1]


def experiment_uri(info, config_writer=True):
    """
    :param bliss.scanning.scan.Scan or dict scan_info:
    :param bool config_writer: expect configurable writer
    :returns str:
    """
    return filenames(info, config_writer=config_writer)[2]
