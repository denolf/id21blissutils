"""
Utilities API exposed to the Bliss session
"""
from gevent import subprocess
from bliss.common.session import get_current_session
from inspect import getdoc, signature
from . import scanutils
from . import config


__all__ = ['helpall', 'helpid21', 'opendata',
           'opendataset', 'opensample', 'openproposal',
           'opendatasets', 'opensamples', 'openproposals']


def _print_short_apidoc(dic, title):
    """
    Print short API documentation

    :param dict dic:
    :param str title:
    """
    docs = {}
    n = 0
    for k, o in dic.items():
        if not callable(o) or k.startswith('_'):
            continue
        doc = getdoc(o)
        if not doc:
            doc = ''
        lst = [s for s in doc.split('\n') if not s.isspace()]
        if lst:
            docs[k] = lst[0].strip()
        else:
            docs[k] = ''
        n = max(n, len(k))
    fmt = '{{:{}}} : {{}}'.format(n)
    print('\n{}:'.format(title))
    print('\n' + '-'*(len(title)+1) + '\n')
    for k, v in sorted(docs.items(), key=lambda kv: kv[0].lower()):
        print(fmt.format(k, v))


def _print_apidoc(dic, title):
    """
    Print API documentation

    :param dict dic:
    :param str title:
    """
    print('\n{}:'.format(title))
    print('\n' + '-'*(len(title)+1) + '\n')
    for k, o in sorted(dic.items(), key=lambda kv: kv[0].lower()):
        if not callable(o) or k.startswith('_'):
            continue
        sig = '{} {}{}'.format(o.__class__.__name__,
                               k, str(signature(o)))
        print('{}:\n'.format(sig))
        doc = getdoc(o)
        if doc:
            print(' ' + '\n '.join(doc.split('\n')))
        print('\n' + '-'*(len(sig)+1) + '\n')


def helpid21(short=True):
    """
    Print ID21 Session Utilities API documentation

    :param bool short:
    """
    from .. import __all__ as id21utils
    dic = {k: v for k, v in get_current_session().env_dict.items()
           if k in id21utils}
    if short:
        _print_short_apidoc(dic, 'ID21 Bliss Utilities')
    else:
        _print_apidoc(dic, 'ID21 Bliss Utilities')


def helpall(short=True):
    """
    Print Bliss Session Utilities API documentation

    :param bool short:
    """
    if short:
        _print_short_apidoc(get_current_session().env_dict, 'Bliss Utilities')
    else:
        _print_apidoc(get_current_session().env_dict, 'Bliss Utilities')


def openuris(uris, block=False):
    """
    Open uri's in silx

    :param list(str): uris
    :param bool block: block session until silx is closed
    """
    uris = [uri + '::/' if '::' not in uri else uri
            for uri in uris if uri]
    print('Opening {} ...'.format(uris))
    if not uris:
        return
    p = subprocess.Popen(['silx', 'view'] + uris,
                         stdout=None, stderr=None)  # NULL?
    if block and p:
        p.wait()


def opendata(scan=None, block=False, config_writer=True):
    """
    Open saved scan result in silx

    :param scans: by default last scan with saving enabled
    :param bool block: block session until silx is closed
    """
    if scan is None:
        uri = scanutils.last_uri(config_writer=config_writer)
        print('last_uri', config_writer, uri)
    else:
        uri = scanutils.scan_uri(scan, config_writer=config_writer)
        print('scan_uri', config_writer, uri)
    openuris([uri], block=block)


def opendatasets(*names, sample=None, proposal=None, block=False,
                 config_writer=True):
    """
    Open datasets in silx

    :param names: dataset names (all by default)
    """
    uris = config.dataset_filenames(*names, sample=sample,
                                    experiment=proposal,
                                    current=False,
                                    config_writer=config_writer)
    openuris(uris, block=block)


def opensamples(*names, proposal=None, block=False, config_writer=True):
    """
    Open samples in silx

    :param names: sample names (all by default)
    """
    uris = config.sample_filenames(*names, experiment=proposal,
                                   current=False, config_writer=config_writer)
    openuris(uris, block=block)


def openproposals(*names, block=False, config_writer=True):
    """
    Open proposals in silx

    :param names: proposal names (all by default)
    """
    uris = config.experiment_filenames(*names, current=False,
                                       config_writer=config_writer)
    openuris(uris, block=block)


def opendataset(block=False, config_writer=True):
    """
    Open current dataset in silx
    """
    uris = config.dataset_filenames(current=True, config_writer=config_writer)
    openuris(uris, block=block)


def opensample(block=False, config_writer=True):
    """
    Open current sample in silx
    """
    uris = config.sample_filenames(current=True, config_writer=config_writer)
    openuris(uris, block=block)


def openproposal(block=False, config_writer=True):
    """
    Open current proposal in silx
    """
    uris = config.experiment_filenames(current=True,
                                       config_writer=config_writer)
    openuris(uris, block=block)
