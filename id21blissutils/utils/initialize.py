"""
Setup in user script
"""

import logging
from nexus_writer_service.metadata import register_all_metadata_generators
from .config import initialize as initialize_config


__all__ = ['id21blissutils_setup']


logger = logging.getLogger(__name__)


def id21blissutils_setup():
    """
    Setup ID21 Bliss utilities (call in user script of the Bliss session)
    """
    logger.debug('Setup ID21 Bliss utilities')
    initialize_config()
    register_all_metadata_generators()
