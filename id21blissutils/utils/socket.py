"""
Common socket utilities
"""

import gevent
from gevent import socket
from gevent import subprocess
import logging
import os
from contextlib import closing
from psutil import process_iter
import signal

logger = logging.getLogger(__name__)

signal_names = {k: v for v, k in signal.__dict__.items()
                if v.startswith('SIG') and not v.startswith('SIG_')}


def hostname():
    return socket.gethostname()


def processes_bound_to_port1(port, notself=True):
    """
    List all processes that are bound to a certain port (use psutils)
    """
    pids = []
    cpid = os.getpid()
    for proc in process_iter():
        for conns in proc.connections(kind='all'):
            if conns.laddr.port == port:
                pid = conns.pid
                if not notself or pid != cpid:
                    pids.append(pid)
    return pids


def processes_bound_to_port2(port, notself=True):
    """
    List all processes that are bound to a certain port (use lsof)
    """
    pids = []
    cmd = 'lsof -t -i:{0}'.format(port)
    response = subprocess.check_output(cmd, shell=True)
    cpid = os.getpid()
    for pid in response.split(b'\n'):
        if pid.isdigit():
            pid = int(pid)
            if not notself or pid != cpid:
                pids.append(pid)
    return pids


processes_bound_to_port = processes_bound_to_port2


def port_islistening(host, port, notself=True, sock=None):
    """
    Check whether processes are bound to a certain port
    """
    try:
        if sock is not None:
            sock.connect((host, port))
        pids = processes_bound_to_port(port, notself=notself)
        islistening = bool(pids)
    except Exception:
        islistening = False
    return islistening


def send_signal(port, sig, portdesc=None, notself=True):
    """
    Send a signal to all processes bound to a specific port
    """
    pids = processes_bound_to_port(port, notself=True)
    if portdesc:
        portdesc = ' listening on ' + portdesc
    else:
        portdesc = ''
    sig_name = signal_names[sig]
    for pid in pids:
        logger.info('Sending {} to pid={}{} ...'
                    .format(sig_name, pid, portdesc))
        os.kill(pid, sig)


def find_free_port():
    """
    Find an unused port
    """
    with closing(socket.socket(
            socket.AF_INET, socket.SOCK_STREAM)) as sock:
        sock.bind(('', 0))
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return sock.getsockname()[1]


def wait_port(port, host=None, open=True, force=False,
              timeout=None, sleep=1, msg=None):
    """
    Wait until a port is open or closed

    :param int port:
    :param str host: localhost by default
    :param bool open: wait until open or closed
    :param bool force: try closing when waiting until closed
    :param int timeout: waiting timeout in seconds
    :param int sleep: interval in seconds
    :param str msg:
    """
    if not msg:
        msg = ''
    if open:
        rstate = 'OPEN'
    else:
        rstate = 'CLOSED'
    if not open and host is None and timeout and force:
        # Try closing a process on locahost
        signals = [None, signal.SIGTERM, signal.SIGKILL]
        timeout = timeout / 3.
    else:
        signals = [None]
    if host is None:
        host = hostname()
    portdesc = '{}:{}{}'.format(host, port, msg)
    with closing(socket.socket(
            socket.AF_INET, socket.SOCK_STREAM)) as sock:
        for sig in signals:
            if sig is not None:
                send_signal(port, sig, portdesc=portdesc, notself=True)
            logger.info('Wait until {} is {} (timeout={})...'
                        .format(portdesc, rstate, timeout))
            with gevent.Timeout(timeout):
                while True:
                    islistening = port_islistening(host, port,
                                                   sock=sock,
                                                   notself=True)
                    if islistening == open:
                        logger.info('{} is {}'
                                    .format(portdesc, rstate))
                        return True
                    else:
                        gevent.sleep(sleep)
    return False
