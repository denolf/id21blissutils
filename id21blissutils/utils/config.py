"""
Bliss session configuration utilities
"""
import os
import re
import logging
from glob import glob
from nexus_writer_service.utils.data_policy import initialize_scan_saving
from bliss.scanning.scan import SCANS
from bliss.common.session import get_current_session
from nexus_writer_service.utils import scan_utils
from nexus_writer_service.utils import config_utils


logger = logging.getLogger(__name__)


__all__ = ['techniques', 'datasets']


def init_scan_saving(scan_saving):
    """
    Initialize session's SCAN_SAVING object (add missing attributes)

    :param bliss.scanning.scan.ScanSaving scan_saving:
    """
    initialize_scan_saving()
    params = {}
    params["technique"] = default_technique()
    params["mdataon"] = False
    for attr, default in params.items():
        try:
            getattr(scan_saving, attr)
        except AttributeError:
            scan_saving.add(attr, default)
    scan_saving.data_filename = '_bliss'
    scan_saving.writer = 'hdf5'  # TODO: This is necessary for setting the device paths


def scan_saving():
    """
    Get session's SCAN_SAVING object

    :returns bliss.scanning.scan.ScanSaving:
    """
    return get_current_session().scan_saving


def initialize():
    """
    Initialize session's configuration
    """
    init_scan_saving(scan_saving())


def last_scans():
    """
    Get global SCANS object

    :returns list(str):
    """
    return SCANS


def scan_saving_pathinfo():
    """
    Get path information from the session's SCAN_SAVING object

    :returns dict:
    """
    # SCAN_SAVING.to_dict() may fail for unrelated reasons
    # so extract only the information we need
    params = {}
    _scan_saving = scan_saving()
    try:
        params['base_path'] = _scan_saving.base_path
    except AttributeError:
        return params
    try:
        params['template'] = template = _scan_saving.template
    except AttributeError:
        return params
    for attr in re.findall(r'\{(.*?)\}', template):
        try:
            params[attr] = getattr(_scan_saving, attr)
        except AttributeError:
            pass
    return params


def scan_saving_path():
    """
    Get path from the session's SCAN_SAVING object

    :returns str:
    :raises RuntimeError: missing information
    """
    params = scan_saving_pathinfo()
    try:
        return os.path.join(params['base_path'],
                            params['template'].format(**params))
    except KeyError as e:
        raise RuntimeError("Missing '{}' attribute in SCAN_SAVING"
                           .format(e))


def scan_saving_get(attr, default=None):
    """
    Get attribute from the session's scan saving object

    :returns str:
    """
    return getattr(scan_saving(), attr, default)


def scan_saving_set(attr, value):
    """
    Get attribute from the session's scan saving object

    :param str attr:
    :param value: must be picklable?
    """
    scan_saving().add(attr, value)


def nexus_definitions():
    """
    Scan saving info from the session configuration

    :returns dict:
    """
    return config_utils.static_root_find("nexus_definitions", default={})


def default_inhouse_name():
    """
    Inhouse folder name from the session configuration

    :returns dict:
    """
    # TODO: somewhere in the static config?
    return 'inhouse'


def nexus_technique_info():
    """
    Information on techniques from the session configuration

    :returns dict:
    """
    return nexus_definitions().get('technique', {})


def default_technique():
    """
    Default technique from the session configuration

    :returns str:
    """
    return nexus_technique_info().get('default', 'undefined')


def current_technique():
    """
    Active technique from the session's scan saving object

    :returns str:
    """
    return scan_saving_get('technique')  # no default needed


def techniques():
    """
    List of available techniques from the session configuration

    :returns list:
    """
    return list(nexus_technique_info().get('techniques', {}).keys())


def samples(config_writer=True):
    """
    Current proposal samples

    :returns list(str):
    """
    lst = []
    searchpattern = scan_utils.filenames(sample='*', config_writer=config_writer)[1]
    if searchpattern:
        for filename in sorted(glob(searchpattern)):
            lst.append(os.path.basename(os.path.dirname(filename)))
    return list(sorted(set(lst)))


def datasets(sample=None, config_writer=True):
    """
    Current proposal samples

    :returns list(str):
    """
    lst = []
    if sample:
        samplepattern = sample
    else:
        samplepattern = '*'
    searchpattern = scan_utils.filenames(sample=samplepattern, dataset='*', config_writer=config_writer)[0]
    if searchpattern:
        for filename in sorted(glob(searchpattern)):
            dataset = os.path.basename(os.path.dirname(filename))
            if sample:
                if dataset.startswith(sample + '_'):
                    dataset = dataset[len(sample)+1:]
            lst.append(dataset)
    return list(sorted(set(lst)))


def dataset_filenames(*names, current=True, sample=None, experiment=None,
                      config_writer=True):
    """
    :param names: dataset names
    :param bool current: use current when no names provided
    :returns list(str)):
    """
    if names:
        if not sample and not current:
            sample = '*'
    else:
        if current:
            names = [scan_saving_get('dataset', '*')]
        else:
            names = ['*']
    lst = []
    kwargs = {}
    if sample:
        kwargs['sample'] = sample
    if experiment:
        kwargs['experiment'] = experiment
    for name in names:
        kwargs['dataset'] = name
        searchpattern = scan_utils.filenames(**kwargs)[0]
        if searchpattern:
            lst += sorted(glob(searchpattern))
    lst = [filename for filename in lst if os.path.basename(filename, config_writer=config_writer) != '_bliss.h5']
    return lst


def sample_filenames(*names, current=True, experiment=None, config_writer=True):
    """
    :param names: sample names
    :param bool current: use current when no names provided
    :returns list(str)):
    """
    if not names:
        if current:
            names = [scan_saving_get('sample', '*')]
        else:
            names = ['*']
    lst = []
    kwargs = {}
    if experiment:
        kwargs['experiment'] = experiment
    for name in names:
        kwargs['sample'] = name
        searchpattern = scan_utils.filenames(sample=name, config_writer=config_writer)[1]
        if searchpattern:
            lst += sorted(glob(searchpattern))
    return lst


def experiment_filenames(*names, current=True, config_writer=True):
    """
    :param names: proposal names
    :param bool current: use current when no names provided
    :returns list(str)):
    """
    if not names:
        if current:
            names = [scan_saving_get('experiment', '*')]
        else:
            names = ['*']
    lst = []
    for name in names:
        searchpattern = scan_utils.filenames(experiment=name, config_writer=config_writer)[2]
        if searchpattern:
            lst += sorted(glob(searchpattern))
    return lst
