import numpy
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.acquisition.motor import SoftwarePositionTriggerMaster
from bliss.scanning.acquisition.motor import VariableStepTriggerMaster
from bliss.scanning.scan import Scan, StepScanDataWatch
from bliss.scanning.chain import AcquisitionChain


__all__ = ['zapline']


def zapline(motor, start, stop, npoints, count_time, *counters, **scan_params):
    """
    :param Axis motor: motor to scan
    :param float start: motor start position
    :param float stop: motor end position
    :param int npoints: the number of points
    :param float count_time: count time (seconds)
    :param counter_args:
            each argument provides counters to be integrated in the scan.
            if no counter arguments are provided, use the active measurement group.
    :param str name: scan name in data nodes tree and directories [default: 'scan']
    :param str title: scan title [default: 'zapline <motor> ... <count_time>']
    :param str save: save scan data to file [default: True]
    :param str save_images: save image files [default: None, means it follows 'save']
    :param str run: if True (default), run the scan. False means just create
                    scan object and acquisition chain
    :param str return_scan: True by default
    :returns Scan:
    """
    # Scan/chain parameters
    scan_params.setdefault("name", 'zapline')
    scan_params.setdefault("save", True)
    if "title" not in scan_params:
        args = ['zapline', start, stop, npoints, count_time]
        template = " ".join(["{{{0}}}".format(i) for i in range(len(args))])
        scan_params["title"] = template.format(*args)
    chain_params = {
        "npoints": npoints,
        "count_time": count_time}
    scan_info = {
        "npoints": npoints,
        "count_time": count_time,
        "start": start,
        "stop": stop,
        "type": scan_params['name'],
        "save": scan_params["save"],
        "title": scan_params["title"],
    }

    # Single-master acquisition
    total_time = count_time * npoints
    chain_master = SoftwarePositionTriggerMaster(
        motor, start, stop, npoints, time=total_time
    )
    #chain_master = VariableStepTriggerMaster(
    #    motor, numpy.linspace(start, stop, npoints)
    #)

    return _create_scan(counters, chain_master, chain_params, scan_params, scan_info)


def _create_scan(counters, chain_master, chain_params, scan_params, scan_info):
    """
    Create acquisition chain and scan object (optionally run the scan)
    """
    # Add detectors to chain
    chain = AcquisitionChain()
    for node in ChainBuilder(counters).get_top_level_nodes():
        acq_params = _get_chain_params(node, chain_params)
        node.set_parameters(scan_params=scan_params,
                            acq_params=acq_params)
        for cnode in node.children:
            acq_params = _get_chain_params(cnode, chain_params)
            cnode.set_parameters(scan_params=scan_params,
                                 acq_params=acq_params)
        chain.add(chain_master, node)
    #print(chain._tree)

    # Create/execute/return scan
    scan = Scan(
        chain,
        name=scan_params['name'],
        save=scan_params['save'],
        save_images=scan_params.get('save_images'),
        scan_info=scan_info,
        scan_saving=None,
        data_watch_callback=StepScanDataWatch(),
    )
    if scan_params.get("run", True):
        scan.run()
    if scan_params.get("return_scan", True):
        return scan


def _get_chain_params(node, chain_params):
    return node._get_default_chain_parameters(chain_params, chain_params)
