import unittest
import gevent
import os
from ...testing.unittest import BlissTestCase
from .. import config


class test_session_api(BlissTestCase):

    def setUp(self):
        super(test_session_api, self).setUp(writer='server',
                                            xrf=True,
                                            xrd=False,
                                            fullfield=False,
                                            keepfiles=False,
                                            managed=True)

    def test_explore_experiment(self):
        env = self.session_env
        detectors = env.simxrfgroup,
        scans = []

        env.newsample('sample1')
        scan = env.ct(0.1, *detectors, save=True)
        scans.append(scan)

        env.newsample('sample2')
        scan = env.ct(0.1, *detectors, save=True)
        scans.append(scan)
        env.newdataset()
        scan = env.ct(0.1, *detectors, save=True)
        scans.append(scan)

        env.newsample('sample3')
        scan = env.ct(0.1, *detectors, save=True)
        scans.append(scan)
        env.newdataset()
        scan = env.ct(0.1, *detectors, save=True)
        scans.append(scan)
        env.newdataset()
        scan = env.ct(0.1, *detectors, save=True)
        scans.append(scan)

        self._wait_scans_finished(scans)

        expected = {'xrf', 'xrfxrd', 'xas', 'fullfield'}
        self.assertEqual(set(env.techniques()), expected)

        expected = {'sample1', 'sample2', 'sample3'}
        self.assertEqual(set(config.samples()), expected)

        expected = {'sample1_0001', 'sample2_0001', 'sample3_0001',
                    'sample2_0002', 'sample3_0002', 'sample3_0003'}
        self.assertEqual(set(env.datasets()), expected)

        expected = {'0001', '0002'}
        self.assertEqual(set(env.datasets('sample2')), expected)

        # Tests opendataset, opensample, etc. without the GUI part:

        proposal = env.SCAN_SAVING.experiment
        base = os.path.join(self.dir.path, proposal, 'id21')
        expected = {os.path.join(base, 'sample3', proposal + '_sample3.h5')}
        self.assertEqual(set(config.sample_filenames()), expected)
        expected = {os.path.join(base, 'sample1', proposal + '_sample1.h5'),
                    os.path.join(base, 'sample3', proposal + '_sample3.h5')}
        self.assertEqual(set(config.sample_filenames('sample1', 'sample3')), expected)
        self.assertEqual(set(config.sample_filenames('sample1', 'sample3', current=False)), expected)
        expected = {os.path.join(base, 'sample1', proposal + '_sample1.h5'),
                    os.path.join(base, 'sample2', proposal + '_sample2.h5'),
                    os.path.join(base, 'sample3', proposal + '_sample3.h5')}
        self.assertEqual(set(config.sample_filenames(current=False)), expected)

        expected = {os.path.join(base, 'sample3', 'sample3_0003', 'sample3_0003.h5')}
        self.assertEqual(set(config.dataset_filenames()), expected)
        expected = {os.path.join(base, 'sample3', 'sample3_0001', 'sample3_0001.h5'),
                    os.path.join(base, 'sample3', 'sample3_0003', 'sample3_0003.h5')}
        self.assertEqual(set(config.dataset_filenames('*0001', '*0003')), expected)
        expected = {os.path.join(base, 'sample3', 'sample3_0001', 'sample3_0001.h5'),
                    os.path.join(base, 'sample3', 'sample3_0002', 'sample3_0002.h5'),
                    os.path.join(base, 'sample3', 'sample3_0003', 'sample3_0003.h5')}
        self.assertEqual(set(config.dataset_filenames(current=False)), expected)
        expected = {os.path.join(base, 'sample2', 'sample2_0001', 'sample2_0001.h5'),
                    os.path.join(base, 'sample2', 'sample2_0002', 'sample2_0002.h5')}
        self.assertEqual(set(config.dataset_filenames(sample='sample2', current=False)), expected)
        expected = {os.path.join(base, 'sample2', 'sample2_0002', 'sample2_0002.h5'),
                    os.path.join(base, 'sample3', 'sample3_0002', 'sample3_0002.h5'),
                    os.path.join(base, 'sample3', 'sample3_0003', 'sample3_0003.h5')}
        self.assertEqual(set(config.dataset_filenames('*0002', '*0003', current=False)), expected)

        expected = {os.path.join(base, proposal + '_id21.h5')}
        self.assertEqual(set(config.experiment_filenames()), expected)


def test_suite():
    """Test suite including all test suites"""
    testSuite = unittest.TestSuite()
    testSuite.addTest(test_session_api("test_explore_experiment"))
    return testSuite


if __name__ == '__main__':
    import sys

    mysuite = test_suite()
    runner = unittest.TextTestRunner()
    if not runner.run(mysuite).wasSuccessful():
        sys.exit(1)
