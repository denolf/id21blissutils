import numpy
from bliss.controllers.counter import CalcCounterController
from bliss.scanning.acquisition.calc import CalcHook


class EvalHook(CalcHook):
    """
    Hook for calculation counter based on a callable
    """

    def __init__(self, name, func, variables):
        """
        :param str name:
        :param callable func: takes variables as named arguments
        :param dict variables: str:str
                               keys are the named arguments in `func`
                               values are the keys of the data `compute` receives
        """
        self._name = name
        self._variables = variables
        self._func = func

    def prepare(self):
        self.data = {}

    def _pop_points(self, nb_points):
        """
        :param int nb_points:
        :returns dict:
        """
        evalkwargs = {self._variables[key]: data[:nb_points]
                      for key, data in self.data.items()}
        self.data = {key: data[nb_points:]
                     for key, data in self.data.items()}
        return evalkwargs

    def compute(self, sender, data_dict):
        """
        :param bliss.scanning.channel.AcquisitionChannel sender:
        :param dict data_dict:
        :returns dict:
        """
        # Append data from counters
        nb_points = []
        for var_name in self._variables:
            var_data = data_dict.get(var_name, [])
            data = self.data.get(var_name, [])
            if len(var_data):
                data = numpy.append(data, var_data)
                self.data[var_name] = data
            nb_points.append(len(data))

        # Do all counters have some data?
        if nb_points:
            nb_points = min(nb_points)
        if not nb_points:
            return

        # Yes they do. Pop that data, apply formula and return
        evalkwargs = self._pop_points(nb_points)
        try:
            with numpy.errstate(divide='ignore', invalid='ignore'):
                ret = self._func(**evalkwargs)
        except ZeroDivisionError:
            # Could be inf as well, but formula should use numpy if you care
            ret = numpy.full(nb_points, numpy.nan)
        return {self._name: ret}


def factory(name, func, counter_dict):
    """
    Create a calculation counter based on a function and some counters

    :param str name:
    :param callable func:
    :param dict counter_dict: str:BaseCounter
                              keys are the named arguments in `func`
    :returns CalcCounterController:
    """
    variables = {ctr.name: var_name for var_name, ctr in counter_dict.items()}
    counters = list(counter_dict.values())
    return CalcCounterController(name, EvalHook(name, func, variables), *counters)


def absorbance(name, it, i0):
    """
    Counter which evaluate -ln(it/i0)

    :param str name:
    :param BaseCounter it:
    :param BaseCounter i0:
    :returns CalcCounterController:
    """
    def func(i0=None, it=None):
        return -numpy.log(it/i0)
    counter_dict = {'i0': i0, 'it': it}
    return factory(name, func, counter_dict)


def ratio(name, a, b):
    """
    Counter which evaluate a/b

    :param str name:
    :param BaseCounter a:
    :param BaseCounter b:
    :returns CalcCounterController:
    """
    def func(a=None, b=None):
        return a/b
    counter_dict = {'a': a, 'b': b}
    return factory(name, func, counter_dict)


def add(name, counters):
    """
    :param str name:
    :param list(BaseCounter) counters:
    :returns CalcCounterController:
    """
    def func(**kwargs):
        return sum(v for v in kwargs.values())
    counter_dict = {c.name: c.name for c in counters}
    return factory(name, func, counter_dict)


def average(name, counters):
    """
    :param str name:
    :param list(BaseCounter) counters:
    :returns CalcCounterController:
    """
    n = len(counters)

    def func(**kwargs):
        return sum(v for v in kwargs.values())/n
    counter_dict = {c.name: c.name for c in counters}
    return factory(name, func, counter_dict)
