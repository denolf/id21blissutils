import unittest
from ..utils.tests import test_all as test_utils
from ..data.tests import test_all as test_data
from ..server.tests import test_all as test_server


def test_suite():
    """Test suite including all test suites"""
    testSuite = unittest.TestSuite()
    testSuite.addTest(test_utils.test_suite())
    testSuite.addTest(test_data.test_suite())
    testSuite.addTest(test_server.test_suite())
    return testSuite


if __name__ == '__main__':
    import sys

    mysuite = test_suite()
    runner = unittest.TextTestRunner()
    if not runner.run(mysuite).wasSuccessful():
        sys.exit(1)
