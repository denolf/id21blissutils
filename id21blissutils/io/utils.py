"""
Common I/O utilities
"""
import os
import errno
import random
import string
import tempfile


def tempname(size=6, chars=string.ascii_letters + string.digits,
             prefix='', suffix=''):
    """
    Random name with prefix and suffix
    """
    # Number of combinations: n^size  (default: 62^6)
    name = ''.join(random.choice(chars) for _ in range(size))
    if not prefix:
        prefix = 'tmp'
    return prefix + name + suffix


def temproot():
    """
    OS tmp directory
    """
    return tempfile.gettempdir()


def tempdir(root=None, **kwargs):
    """
    Random directory in OS tmp directory
    """
    if not root:
        root = temproot()
    return os.path.join(root, tempname(**kwargs))


def mkdir(path):
    """
    Create directory recursively and silent when already existing
    """
    try:
        if path:
            os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise
