ID21 bliss utilities
====================

THIS PROJECT WAS INCORPORTATED IN THE MAIN BLISS PROJECT (NEXUS WRITER).

Installation
------------

Within your Bliss Conda environment

.. code-block:: bash

    git clone https://gitlab.esrf.fr/denolf/id21blissutils.git
    python -m pip install --no-deps id21blissutils

Import this library in the user script of your bliss session

.. code-block:: python

    from id21blissutils import *
    id21blissutils_setup()

The user script is often located in "/users/blissadm/local/beamline_configuration/sessions/scripts/session_<name>.py".

Usage in Bliss session:
-----------------------

Print the utility documentation:

.. code-block:: python

    UT_SESSION_1 [1]: helpall()
    UT_SESSION_1 [1]: helpid21()

Change data location (use `newinhouse`, `newvisitor` or `newexperiment`):

.. code-block:: python

    UT_SESSION_1 [3]: newexperiment('test')
    UT_SESSION_1 [4]: newsample('sample1')
    UT_SESSION_1 [5]: newdataset('area1')
    UT_SESSION_1 [6]: saveinfo()
    Bliss saving info : 
    -------------------
    Status         : COMPLETE
    Path           : /data/id21/inhouse/default/id211908/id21/sample1/sample1_area1
    Template       : {experiment}/{beamline}/{sample}/{dataset}
    Base_path      : /data/id21/inhouse/default
    Experiment     : id211908
    Beamline       : id21
    Technique      : xrf
    Sample         : sample1
    Dataset        : sample1_area1
    Metadatamanager: ENABLED (SYNCHRONIZED)
    -------------------


Usage in Bliss Conda environment:
---------------------------------

Start an external HDF5 writer:

.. code-block:: bash

    hpc2-0701:wout/dev/id21blissutils % python -m id21blissutils.server.nxscanwriter <session_name> --log=info

Setup a standalone test environment (files + servers):

.. code-block:: bash

    xrmh:wout/dev/id21blissutils % python -m id21blissutils.testing.main --webapp

    Bliss Test Environment is setup in '/tmp/tmpqpqgirpt'
    Connect to Bliss session like this:

     BEACON_HOST=xrmh:37836 TANGO_HOST=xrmh:52244 bliss -s test_session_1 --no-tmux --log WARN

    Start Nexus writer like this:

     BEACON_HOST=xrmh:37836 TANGO_HOST=xrmh:52244 python -m id21blissutils.server.nxscanwriter test_session_1

    Connect to Redis like this:

     redis-cli -s /tmp/redis_eY29bW.sock -n 0

    Connect to Tango database like this:

     TANGO_HOST=xrmh:52244 jive

    Connect to Beacon webapp like this:

     firefox xrmh:52973

    Press any key to stop the servers


Run unit tests:

.. code-block:: bash

    hpc2-0701:wout/dev/id21blissutils % python -m id21blissutils.tests.test_all --log=info