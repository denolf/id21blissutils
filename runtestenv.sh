if [[ ! -z $(command -v activatebliss) ]]; then
    activatebliss
fi
if [[ -z $(command -v bliss) ]]; then
    echo "Run in Bliss conda environment"
else
    python -m id21blissutils.testing.main "$@"
fi
