Change Log
==========

`master`_ (unreleased)
----------------------

* Move writer code to the Bliss project
* Bliss session API:
    * newdefaultexperiment, newtmpexperiment
    * newexperiment is replaced by newlocalexperiment


`v0.0.1b0`_ (unreleased)
------------------------

* Nexus writer:
    * Split simple writer (no extra Redis information) and
      configurable writer (extra Redis information)
    * Add NXdata grid plots (using HDF5 virtual datasets)


`v0.0.1a0`_ (unreleased)
------------------------

* Bliss test environment:
    * Beacon server
    * YAML database (create or use existing)
    * Lima Tango servers
* Bliss session API:
    * newvisitor, newinhouse, newexperiment, closeexperiment
    * newsample, newdataset, switchtechnique
    * definesample, sample
    * samples, datasets, techniques
    * saveinfo, msaveinfo
    * elog, mdataon, mdataoff
    * mdatapull, mdatapush, mdatacompare
    * opendata, opendataset. opensample, openproposal
    * opendatasets. opensamples, openproposals
    * helpid21, helpall
* Nexus writer:
    * channels (0D, 1D)
    * lima (2D)
    * application definitions (NXxrf)
    * timer channels as positioner


`genesis`_ (unreleased)
-----------------------

* Creation


.. _genesis: https://gitlab.esrf.fr/denolf/id21blissutils/tags/genesis
.. _v0.0.1a0: https://gitlab.esrf.fr/denolf/id21blissutils/compare/genesis...v0.0.1a0
.. _v0.0.1b0: https://gitlab.esrf.fr/denolf/id21blissutils/compare/v0.0.1a0...v0.0.1b0
.. _master: https://gitlab.esrf.fr/denolf/id21blissutils/compare/v0.0.1b0...master
