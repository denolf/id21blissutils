#
# source runtests.sh --log debug
#


function _run()
{
    # ALL
    #python setup.py test
    #python -m id21blissutils.tests.test_all
    #python -m unittest id21blissutils.tests.test_all.test_suite -v
    #pytest

    # SELECTED
    #python -m id21blissutils.data.tests.test_blissdata "$@"
    #python -m id21blissutils.data.tests.test_session_api "$@"
    python -m id21blissutils.server.tests.test_scan_writer "$@"
    #python -m id21blissutils.data.tests.test_all "$@"
    #python -m id21blissutils.utils.tests.test_session_api "$@"
    #python -m id21blissutils.utils.tests.test_data_merging "$@"
    #python -m unittest id21blissutils.server.tests.test_scan_writer.test_scan_writer.test_ct -v
    #python -m unittest id21blissutils.data.tests.test_session_api.test_session_api.test_experiment -v
    #python -m unittest id21blissutils.data.tests.test_blissdata.test_blissdata.test_experiment -v

    # SCRIPTS
    #python -m id21blissutils.client.metadata

    # GDB
    #gdb -ex 'break Tango::Util::connect_db' --args python -m id21blissutils.server.tests.test_scan_writer

    #(gdb) target exec python
    #(gdb) run
    #>>> import unittest;from id21blissutils.server.tests.test_scan_writer import test_suite;mysuite = test_suite()
    #>>> runner = unittest.TextTestRunner()
    #>>> runner.run(mysuite)
    #>>> [C-c]
    #(gdb) info functions connect_db
    #(gdb) b Tango::Util::connect_db
    #(gdb) catch throw
    #(gdb) info breakpoints
    #(gdb) cont
    #(gdb) info variables
    #(gdb) info locals
    #(gdb) info args
    #(gdb) delete breakpoint 2

    return $?
}


function _loop()
{
    # Repeat (overwrite output log) until it fails:
    rm runtests1.log
    rm runtests2.log
    for i in {1..5000}; do
        _run "$@" 2>&1 | tee runtests1.log
        if [[ $PIPESTATUS != 0 ]]; then
            echo 'FAILED on runtests1.log'
            break
        fi
        _run "$@" 2>&1 | tee runtests2.log
        if [[ $PIPESTATUS != 0 ]]; then
            echo 'FAILED on runtests2.log'
            break
        fi
    done
}


if [[ ! -z $(command -v activatebliss) ]]; then
    activatebliss
fi
if [[ -z $(command -v bliss) ]]; then
    echo "Run in Bliss conda environment"
else
    #export PYTHONWARNINGS="ignore"
    _run "$@"
    #_loop "$@"
    #_loop --log=debug
    #unset PYTHONWARNINGS
fi
